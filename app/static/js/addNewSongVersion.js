// https://stackoverflow.com/questions/11100821/javascript-regex-for-validating-filenames/27351102
var isValid=(function(){
var rg1=/^[^\\/:\*\?"<>\|]+$/; // forbidden characters \ / : * ? " < > |
var rg2=/^\./; // cannot start with dot (.)
var rg3=/^(nul|prn|con|lpt[0-9]|com[0-9])(\.|$)/i; // forbidden file names
return function isValid(fname){
  return rg1.test(fname)&&!rg2.test(fname)&&!rg3.test(fname);
}
})();

function addNewSongVersion(songName, allVersions)
{
  var newVersion = document.getElementById("newVersion").value

  var validName = 1;
  for(i = 0; i < allVersions.length; i++)
  {
    if(allVersions[i] == newVersion)
    {
      validName = 0;
      document.getElementById("songVersionErrorMessage").innerHTML = "Please choose a name not taken already"
    }
  }
  // Check we can use the name for a directory
  var regEx = isValid(newVersion)
  if(regEx == false)
  {
    validName = 0
    document.getElementById("songVersionErrorMessage").innerHTML = "Unfortunately this name cannot be used for a directory"
  }

  // Send an AJAX request to make a new folder and to redirect
  if(validName == 1)
  {
    document.getElementById("songVersionErrorMessage").innerHTML = ""

    console.log("/chooseSong/" + songName)
    //Send back the data
    //So flask can save the file and update it
    $.ajax({
      type: "POST",
      contentType: "application/json;charset=utf-8",
      url: "/updateVersions/".concat(songName, "/", newVersion),
      traditional: "true",
      data: null,
      dataType: "json"
    }).done(window.location.href = ("/chooseSong/" + songName));


  }
}
