# Fractals

Final Project Year 3


This GitHub repository comprises of


Ray Marching Prototype
----------------------------

Python 3 and Pygame required

to run

python3 main.py


Fast Fourier Transform Tests
----------------------------

Python3, numpy, matplotlib, scipy, pygame, pylab required

to run drum tests

python3 drumAnalysis.py

to run bass tests

python3 analyseFFT.py


MIDI
----------------------------

Python3 and MIDO required

to run MIDI anaylsis

python3  generateMetadata.py # runs for all songs

python3  generateMetadata.py  "Song Name" # for specific song


Animation Data Generator
----------------------------

Python3 required

python3 animDataGenerator.py # for all songs and versions at once

python3 animDataGenerator.py "Song Name" "Version name"  # for specific version


Data
----------------------------

Contains the data for every song, along with the images 
and verisons. 

Flask
----------------------------

Python3 and flask required 
To run stay on top level directory and use

flask run


3D Ray Marching
----------------------------

CPP, OpenGL, irrKlang, ffMPEG, GLAD, GLFW
must be installed

to run

make
./MusicVisualiser


Notes
----------------------------
I imagine getting this to work on another 
computer will be pretty difficult.

