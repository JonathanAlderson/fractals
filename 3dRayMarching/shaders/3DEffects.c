//---
// Alien Sphere which deforms to music
// Basic Spheres with rotating light
// Colour: Colour of the main sphere
// Intensity: Displacement effect
// Effect: 1. Repeating in X Axis, 2. Inside Sphere, 3. Weaving between moving spheres
// Displacement Intensity: The complexity of the displacement
//---
vec3 sphereScene(vec2 uv, float colour, float intensity, int argIndex)
{

  float funcNum = 1.0; // Since this is the first 3D function
  vec3 lightPos = vec3(0.0, 4.0, -20.);


  vec3 start = vec3(0.0, .2, -8.0);
  vec3 direction = normalize(vec3(uv.x, uv.y, 1.0)); // looking into positive z direction

  float distance = rayMarch(start, direction, funcNum, argIndex); // 1.0 tells the Ray March Alg to use the distSphereScene function as the Corresponding distance function

  vec3 finalPoint = start + direction * distance;

  // Colouring
  vec3 col = vec3(0.);


  float ambient = 0.0;
  if(distance < MAX_DIST)
  {
    vec3 p = vec3(finalPoint.x, finalPoint.y, finalPoint.z)+time*.5;
    float pattern;
    float cutoff = 1.0 - intensity;
    pattern = smoothstep(cutoff, 1.0, snoise(p));
    pattern += smoothstep(cutoff, 1.0, snoise(p*2.));
    pattern += smoothstep(cutoff, 1.0, snoise(p*4.));
    col = doCol(colour+50.*pattern);

    ambient = .8;
    col = col * (ambient + doLighting(finalPoint, 1.0, lightPos, argIndex));
  }

  return vec3(col);
}

//---
// Rave Spheres
// For use only in raves
// Colour: The colour of the effect
// Effect: 1-5 differnet pattern variations
// Colour Cycle: Set to 1 for colours to cycle
// Intensity: Changes the colour by the intensity
//---
vec3 raveSpheres(vec2 uv, float colour, float effect, float colourCycle, float intensity, int argIndex)
{
  colour += intensity;
  if(colourCycle == 1.0){ colour +=time; }

  float funcNum = 2.0; // Since this is the first 3D function

  vec3 lightPos = vec3(0.0, 5.0, 0.0);
  vec3 start = vec3(5.0, 5.0, -5.0);
  vec3 direction = normalize(vec3(uv.x, uv.y, 1.0)); // looking into positive z direction

  float distance = rayMarch(start, direction, funcNum, argIndex); // 1.0 tells the Ray March Alg to use the distSphereScene function as the Corresponding distance function
  vec3 finalPoint = start + direction * distance;
  float ambient = 0.0;

  vec3 col = vec3(0.);
  // Coloring
  if(distance < MAX_DIST)
  {
    ambient = .8;
    col = doCol(colour) * (ambient + doLighting(finalPoint, 1.0, lightPos, argIndex));
  }

  return vec3(col);
}


//---
// Chains
// Infinite Chains the user walks through
// Colour: the primary colour of the effect
// Intensity: what controls the colour intensity
// Nausea: How nauseating it is
//---
vec3 chains(vec2 uv, float colour, float intensity, float nausea, int argIndex)
{
  float funcNum = 3.0; // Since this is the third 3D function

  vec3 col = vec3(0.0);
  vec3 start = vec3(0.0, 5.0, -5.0);

  vec3 direction = normalize(vec3(uv.x, uv.y, 1.0)); // looking into positive z direction

  float distance = rayMarch(start, direction, funcNum, argIndex); // 1.0 tells the Ray March Alg to use the distSphereScene function as the Corresponding distance function

  vec3 finalPoint = start + direction * distance;

  if(distance < MAX_DIST)
  {
    col = doCol(colour);
    col = col * vec3(distance/ 10.) * intensity;
    col += .5 * getNormal(finalPoint, funcNum, argIndex);
  }

  return vec3(col);
}




//---
// Repeating Cubes
// Infinite Cubes That React to things
// Colour: the primary colour of the effect
// Intensity: what controls the colour intensity
// Nausea: How nauseating it is
//---
vec3 pulsingCubes(vec2 uv, float colour, float intensity, int argIndex)
{
  float funcNum = 4.0; // Since this is the third 3D function


  vec3 col = vec3(.0);
  vec3 start = vec3(0.0, 5.0, -5.0);


  vec3 direction = normalize(vec3(uv.x, uv.y, 1.0)); // looking into positive z direction

  float distance = rayMarch(start, direction, funcNum, argIndex); // 1.0 tells the Ray March Alg to use the distSphereScene function as the Corresponding distance function

  vec3 finalPoint = start + direction * distance;

  if(distance < MAX_DIST)
  {
    col = doCol(colour); // Just get the first colour for now
    vec3 norm = getNormal(finalPoint, funcNum, argIndex);
    col = col * vec3(norm.y + norm.x) + distance/100.;
  }

  return vec3(col);
}



//---
// Wave of Cubes
// Cubes moving up and down with the music
// Colour: Colour of the cubes
// Height: How tall the cubes are
// Speed: Speed the noise moves through the waves
// Sampling: How often the wave is sampled the lower the smoother
// Variance: How much impact the noise has on the final height
// Effect: 1. Colour changes over time 2.Z Axis rotate 3. Both
//---
vec3 waveofCubes(vec2 uv, float colour, float effect, int argIndex)
{
  if(effect == 1.0 ||	effect == 3.0){ colour += time; }

  float funcNum = 5.0; // Tells us which Ray Marching function to run

  vec3 col = vec3(.0);
  vec3 start = vec3(0.0, 0.0, -5.0);

  vec3 direction = normalize(vec3(uv.x, uv.y, 1.0)); // looking into positive z direction

  float distance = rayMarch(start, direction, funcNum, argIndex); // 1.0 tells the Ray March Alg to use the distSphereScene function as the Corresponding distance function

  vec3 finalPoint = start + direction * distance;

  if(distance < MAX_DIST)
  {
    col = doCol(colour); // Just get the first colour for now

    vec3 norm = getNormal(finalPoint, funcNum, argIndex);
    col = col * vec3(norm.y + norm.x) + distance/100.;
  }


  return vec3(col);
}

//---
// 3D Fractal
// Iteresting 3D Fractal made out of Tetrahedrons
// Colour: Main Colour of the fractal
// Speed: How fast it moves
// Start: The start position of the movement
// Effect: 1. Rotate in Z axis 2. Rotate in X and Z axis 3. Differnet Fractal Rotate Effect
// Reps: How many iterations the fractal has (the greater the slower it runs)
// Size: How big the fractal is
//---
vec3 threeDFractal1(vec2 uv, float colour, int argIndex)
{
  float funcNum = 6.0;

  vec3 col = vec3(.0);
  vec3 start = vec3(0.0, 0.0, -10.0);

  vec3 direction = normalize(vec3(uv.x, uv.y, 1.0)); // looking into positive z direction
  float distance = rayMarch(start, direction, funcNum, argIndex); // tells the Ray Marcher to use the right distance function
  vec3 finalPoint = start + direction * distance;

  if(distance < MAX_DIST)
  {
    col = doCol(colour * distance);
    vec3 norm = getNormal(finalPoint, funcNum, argIndex);
    col = col * vec3(norm.y + norm.x) + distance/100.;
  }
  return vec3(col);
}


//---
// Morphing Shapes
// Iteresting 3D Fractal made from Cubes
// Colour: Main Colour of the fractal
// Colour Repeats: How often the colours cycle
// Spacing: How far apart each cube is
// Size: How large the rounded boxes are
// Iterations: How many reflection iterations there are
// X Rotate: How much x Rotation happends each iteration
// Y Rotate: How much y Rotation happends each iteration
//---
vec3 threeDFractal2(vec2 uv, float colour, float colRepeats, int argIndex)
{
  float funcNum = 7.0;

  vec3 col = vec3(.0);
  vec3 start = vec3(0.0, 0.0, -15.0);

  vec3 direction = normalize(vec3(uv.x, uv.y, 1.0)); // looking into positive z direction
  float distance = rayMarch(start, direction, funcNum, argIndex); // tells the Ray Marcher to use the right distance function
  vec3 finalPoint = start + direction * distance;

  if(distance < MAX_DIST)
  {
    col = doCol((colour * distance)*colRepeats);
    vec3 norm = getNormal(finalPoint, funcNum, argIndex);
    col = col * vec3(norm.y + norm.x) + distance/100.;
  }
  return vec3(col);
}
