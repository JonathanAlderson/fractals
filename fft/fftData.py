"""
Finds the FFT data for any frame in a wav file
This allows us to get the volume over the track
and the frequency distribution aswell
"""
import sys
import numpy
import time
import math
import matplotlib.pyplot as plt
from random import sample
import pylab
import os
from scipy.io import wavfile
from scipy.fftpack import fft


class FFTData:

    def __init__(self, songname, directory = "../data/"):
        """ Loads in the wav file """
        self.songName = songname
        self.valid = 0
        self.directory = directory

        if(self.directory == ""):
            self.filename = self.songName
            self.valid = 1
            self.samplingFreq, self.fullSound = wavfile.read(self.filename)
        else:
            # Allows us to run the progam from multiple places
            if(os.getcwd().endswith("fractals")):
                self.directory = self.directory[3:]
                
            print(self.directory + self.songName)
            for file in os.listdir(self.directory + self.songName):
                print(file)
                if file.endswith(".wav"):
                    self.valid = 1
                    self.filename = file
                    print("Found wav " + str(self.filename))

            if(self.valid != 0):
                self.samplingFreq, self.fullSound = wavfile.read(self.directory + self.songName + "/" + self.filename)

    def normalizeList(self, a):
        """ Puts all the values in the list between a 0 and 1 range """
        mi = min(a)
        ma = max(a)
        if(mi == ma): # Then everything is the same
            return [0.5 for i in a]
        else:
            for i in range(len(a)):
              a[i] = ((a[i] - mi) / (ma - mi))
            return a

    def getVolume(self):
        """ Will be able to tell you how the volume changes over the course of the file """
        pollsPerSecond = 10
        targetFrames = 1/60
        volumes = []
        minVolume = -70 # If it's less than this we don't care, just count it as the same
                        # or else the results get ruined by something -30 dB diff when they're both as quiet
        i = 0
        j = 0
        while (i * self.samplingFreq < len(self.fullSound)):
            j += 1
            a = self.getFFTData(i, i + 1 / 60)
            i += 1 / pollsPerSecond

            maxVol = max(a[1])
            if(maxVol < minVolume):
                maxVol = minVolume

            volumes.append(maxVol)
        volumes = self.normalizeList(volumes)
        volumes = self.getFrameData(volumes,int((1/pollsPerSecond)/targetFrames))
        return volumes

    def getFrequencyDistriution(self):
        """ Will be able to tell you the frequency distribution of the song
            for each frame                                                  """
        pollsPerSecond = 10
        freqBands = 10
        targetFrames = 1/60
        freq = []
        minVolume = -70 # If it's less than this we don't care, just count it as the same
                        # or else the results get ruined by something -30 dB diff when they're both as quiet
        i = 0
        j = 0
        while (i * self.samplingFreq < len(self.fullSound)):
            j += 1
            a = self.getFFTData(i, i + 1 / 60)[1]
            i += 1 / pollsPerSecond
            for k in range(len(a)):
                if(a[k] < minVolume):
                    a[k] = minVolume
            # Split frequency space into 10 groups
            # Average the volume of each group
            # Average the averages to see where the average freq is
            gSize = int(len(a)/freqBands) # how many in each group
            splitFreq = [a[i*gSize:i*gSize+gSize] for i in range(math.floor(len(a)/gSize))]
            splitFreq = splitFreq[0:freqBands]
            splitFreq = [sum(splitFreq[i])/len(splitFreq[i]) for i in range(len(splitFreq))]
            splitFreq = self.normalizeList(splitFreq)
            thisFreq = sum(splitFreq)/len(splitFreq)
            freq.append(thisFreq)

        freq = self.normalizeList(freq)
        freq = self.getFrameData(freq,int((1/pollsPerSecond)/targetFrames))
        return freq

    def getFrameData(self, data, numInterps):
        """ Changes a series of data from one time scale to another """
        frameData = []
        for i in range(len(data)-1):
            for j in range(numInterps):
                frameData.append(self.lerp(data[i], data[i+1], j/numInterps))
        frameData.append(data[-1])
        return frameData

    def lerp(self, a, b, c):
        """ Lerps between a and b with a value of c """
        return(round((b - a) * c + a, 3))

    def getFFTData(self, start, end):
        """ Allows you to get the data for any range of frames in the data """

        mySound = self.fullSound[int(start * self.samplingFreq): int(end * self.samplingFreq)]

        #Check if wave file is 16bit or 32 bit. 24bit is not supported
        mySoundDataType = mySound.dtype

        #We can convert our sound array to floating point values ranging from -1 to 1 as follows
        mySound = mySound / (2.**15)

        #Check sample points and sound channel for duel channel(5060, 2) or  (5060, ) for mono channel
        mySoundShape = mySound.shape
        samplePoints = float(mySound.shape[0])
        #Get duration of sound file
        signalDuration =  mySound.shape[0] / self.samplingFreq
        #If two channels, then select only one channel
        mySoundOneChannel = mySound[:,0]
        #Get length of mySound object array
        mySoundLength = len(mySound)
        #Take the Fourier transformation on given sample point
        fftArray = fft(mySoundOneChannel)
        numUniquePoints = int(numpy.ceil((mySoundLength + 1) / 2.0))

        fftArray = fftArray[0:numUniquePoints]

        #FFT contains both magnitude and phase and given in complex numbers in real + imaginary parts (a + ib) format.
        #By taking absolute value , we get only real part
        fftArray = abs(fftArray)


        #Scale the fft array by length of sample points so that magnitude does not depend on
        #the length of the signal or on its sampling frequency
        fftArray = fftArray / float(mySoundLength)

        #FFT has both positive and negative information. Square to get positive only
        fftArray = fftArray **2

        #Multiply by two
        #Odd NFFT excludes Nyquist point
        if mySoundLength % 2 > 0: #we've got odd number of points in fft
            fftArray[1:len(fftArray)] = fftArray[1:len(fftArray)] * 2

        else: #We've got even number of points in fft
            fftArray[1:len(fftArray) -1] = fftArray[1:len(fftArray) -1] * 2

        freqArray = numpy.arange(0, numUniquePoints, 1.0) * (self.samplingFreq / mySoundLength);

        return(freqArray/1000, 10 * numpy.log10(fftArray))

    def makeFFTMetadata(self):
        song = self.songName

        if(self.valid == 1):
            vol = self.getVolume()
            freq = self.getFrequencyDistriution()

            with open(self.directory + song + "/fftMetadata.txt", "w+") as f:
                for i in range(min(len(vol), len(freq))):
                    f.write(str(vol[i]) + " " + str(freq[i]))
                    f.write("\n")
            return("FFT Data Generated for " + str(self.songName))
        else:
            return("No WAV file for " + str(self.songName))

def getAllSongs():
    """ Gets every song in data"""
    directory = "../data/"
    if(os.getcwd().endswith("fractals")):
        directory = directory[3:]

    songs = [x[1] for x in os.walk(directory)][0]
    return songs

def generateAllFFTData():
    """ Goes through and finds every song, and every song version
        and updates all their animation data. This can help us
        find if any of the strucutre data files are broken       """
    returnMessage = ""
    s = getAllSongs()

    for i in s:
        print(i)
        #try:
        a = FFTData(i)
        returnMessage += a.makeFFTMetadata()
        #except:
        #returnMessage += str(i + " failed")
        returnMessage += "\n"
    return returnMessage

def main():
    # Tries to run for a single file,
    # if no arguments runs them all

    if(len(sys.argv) == 2 and sys.argv[1] != "run"):
        a = FFTData(sys.argv[1])
        a.makeFFTMetadata()
    else:
        return(generateAllFFTData())


if __name__ == "__main__":
    main()
