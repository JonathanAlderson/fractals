// https://stackoverflow.com/questions/11100821/javascript-regex-for-validating-filenames/27351102
var isValid=(function(){
var rg1=/^[^\\/:\*\?"<>\|]+$/; // forbidden characters \ / : * ? " < > |
var rg2=/^\./; // cannot start with dot (.)
var rg3=/^(nul|prn|con|lpt[0-9]|com[0-9])(\.|$)/i; // forbidden file names
return function isValid(fname){
  return rg1.test(fname)&&!rg2.test(fname)&&!rg3.test(fname);
}
})();

function addNewCustomAutomation(allNames)
{
  console.log("Add new custom automatios")
  console.log(allNames)
  var newName = document.getElementById("newName").value
  console.log(newName)

  var validName = 1;
  for(i = 0; i < allNames.length; i++)
  {
    if(allNames[i] == newName)
    {
      validName = 0;
      document.getElementById("customAutomationErrorMessage").innerHTML = "Please choose a name not taken already"
    }
  }
  // Check we can use the name for a directory
  var regEx = isValid(newName)
  if(regEx == false)
  {
    validName = 0
    document.getElementById("customAutomationErrorMessage").innerHTML = "Unfortunately this name cannot be used for a automation name"
  }

  // Send an AJAX request to make a new folder and to redirect
  if(validName == 1)
  {
    document.getElementById("customAutomationErrorMessage").innerHTML = ""

    //Send back the data
    //So flask can save the file and update it
    $.ajax({
      type: "POST",
      contentType: "application/json;charset=utf-8",
      url: "/updateCustomAutomations/".concat(newName),
      traditional: "true",
      data: null,
      dataType: "json"
    }).done(window.location.href = ("/viewAllCustomAutomations"));


  }
}
