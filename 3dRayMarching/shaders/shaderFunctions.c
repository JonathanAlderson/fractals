#version 330 core
#define PI 3.1415926535897932384626433832795
#define MAX_DIST 100.
out vec4 FragColor;
in vec4 gl_FragCoord;
in vec3 ourColor;
in vec2 TexCoord;
uniform sampler2D ourTexture;
uniform vec2 screen;
uniform float time;
uniform float rayMarchingSteps;


// Uniforms for all the functions we need
uniform float[30] effectScreen;
uniform float[30] effectBackG;
uniform float[30] effect3D;
uniform float[30] effectFilter;
uniform float[15] colours;

///////////////////////////////////////
// FOLD SPACE AROUND A LINE
//////////////////////////////////////
vec2 fold(vec2 p, float ang)
{
  // The normal of the line we are reflecting about
  vec2 normal = vec2(cos(-ang), sin(-ang));

  // Distance to the line, used in the reflection
  float distToLine = dot(p, normal);

  // We only want to reflect one side, the other stays the same
  distToLine = min(0., distToLine);

  // Take 2 steps to get the distance
  p -= 2 *distToLine * normal;

  return p;
}

///////////////////////////////////////
// SMOOTH SIN IN RANGE
//////////////////////////////////////
float mySin(float min, float max, float offset, float freq)
{
    return ((sin((time * freq) + offset)+1.)*.5)*(max-min)+min;
}


///////////////////////////////////////
// Resolves the colour from a single int, to the full thing
// but also does colour blending, very clever stuff. So when we do
// 1.2 is actually lerps
//////////////////////////////////////
vec3 doCol(float col)
{
  col =  mod((col - 1.0), 5.0);          // Change indexing

  int a = int(mod(floor(col), 5.0)*3.0); // Find array indexes
  int b = int(mod(ceil(col), 5.0)*3.0);
  float c = fract(col);                  // The mix amount

  vec3 aCol = vec3(colours[a]/255., colours[a+1]/255., colours[a+2]/255.);
  vec3 bCol = vec3(colours[b]/255., colours[b+1]/255., colours[b+2]/255.);
  return mix(aCol, bCol, c);
}

///////////////////////////////////////
// Resolves the complementary colour from a single int, to the full thing
//////////////////////////////////////
vec3 doColComp(float col)
{
  vec3 colour = doCol(col);
  return vec3(1. - colour.x, 1. - colour.y, 1. - colour.z);
}

///////////////////////////////////////
// Kaeilidoscopic Fold
//////////////////////////////////////
vec2 kaelidoscopeFold(vec2 uv, float iterations, float foldAmount)
{
  for(int i = 0; i < iterations; i ++)
  {
    // Mirror Horizontally
    // Move line to the right
    // Mirror across Pi/6
    uv = fold(uv, 0);
    uv.x -= .5;
    uv = fold(uv, foldAmount);

    // Rescale Coords
    if(i < iterations-1)
    {
      uv *= (3.);
      uv.x -= (1.5);
    }
  }
  return uv;
}


///////////////////////////////////////
// Smooth Range
// Smoothly interpolates between a minimum and a
// maximum, between the range of 0 to 1. Very useful
// for shader programming
//////////////////////////////////////
float smoothrange(float min, float max, float amount)
{
  return min + (max-min)*smoothstep(.0, 1., amount);
}

///////////////////////////////////////
// Like the rotation matrix we've made in graphics
//////////////////////////////////////
vec2 rot(vec2 p, float angle)
{
  float s = sin(angle);
  float c = cos(angle);
  return(vec2(p.x*c + p.y*s,p.y*c-p.x*s));
}
