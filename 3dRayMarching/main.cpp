// Includes
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "stb_image.h"
#include "stb_image_write.h"
#include <iostream>
#include <cstring>
#include <string>
#include <vector>
#include <tuple>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <ctime>
#include <thread>
#include <future>
#include <unistd.h>
#include "loadFiles.h"
#include "glSetup.h"



#include <irrKlang/irrKlang.h>
#include <irrKlang/ik_ISound.h>

#include "irrKlang/examples/common/conio.h"

// Combine all our shader files together
// So they can be compiled together
std::string content = makeShaders();
const char *fragmentShaderSource = content.c_str();

// settings
int SCR_WIDTH = 800;
int SCR_HEIGHT = SCR_WIDTH * 0.5625;  // 16 : 9

// Draws green to screen
void clearScreen()
{
  glClearColor(0.f, 0.6f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
}

void clearSections()
{
  // Ran at the start to delete any spare frames
  std::system("> ../videos/sections.txt");
}

void imagesToVideoSegment(std::string command, float record, std::string frame)
{
  // Converts the last 60 frames to a video and
  // deletes the frames
  std::cout << "recording" << std::endl;
  if(record == 1.0)
  {

    std::system(command.c_str());
    std::system("find ../videos/frame* -exec rm {} \\;");
    std::system(("echo \"file 'section" + frame + ".mp4'\" >> ../videos/sections.txt").c_str());
  }
}

void saveVideo(std::string mergeVideo, std::string addAudio, float record)
{

  std::cout << "doing audio" << std::endl;

  if(record == 1.0)
  {
    std::system(mergeVideo.c_str());
    std::system(addAudio.c_str());
    std::system("find ../videos/section* -exec rm {} \\;");
  }
}


int saveScreenshot(const char *filename)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    int x = viewport[0];
    int y = viewport[1];
    int width = viewport[2];
    int height = viewport[3];

    char *data = (char*) malloc((size_t) (width * height * 3)); // 3 components (R, G, B)

    if (!data)
        return 0;

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadPixels(x, y, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);

    int saved = stbi_write_png(filename, width, height, 3, data, 0);

    free(data);

    return saved;
}

// MAIN
int main()
{

    // Settings
    float playSong = 1.0;
    float playbackSpeed = 1.;
    float sync = .1;
    float fullscreen = 0.0;
    float wait = 0.0; // how many seconds we wait before starting, useful for filming
    float timeOffset = 0.0 + sync;

    // Render Settings
    std::string framerate;
    float record;
    float rayMarchingSteps;

    // Read All Settings In From Text Files
    std::string song = "";
    std::string version = "";
    std::string startTime = "";
    std::string playbackSpeedString = "";
    tie(song, version, startTime, playbackSpeedString) = readSettings("settings.txt");
    std::tie(fullscreen, wait, sync, playSong, framerate, record, rayMarchingSteps) = readRenderSettings("renderSettings.txt");
    std::string mp3File =  "../data/" + song + "/" + song + ".mp3";
    std::string imageFile =  "../data/" + song + "/images/allImages.txt";


    // Screen Recording Setup
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];
    time (&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer,sizeof(buffer),"_%d-%m-%Y_%H:%M",timeinfo);
    std::string currentTime(buffer);
    std::string audioCommand = "ffmpeg -y -i ../videos/'" + song + "'" + currentTime + ".mp4"  " -i ../data/'" + song + "'/'" + song + "'.mp3 -c copy -crf 18 ../videos/'" + song + "'" + currentTime + "_withAudio.mp4";
    std::string joinVideoCommand = "ffmpeg -y -f concat -safe 0 -i ../videos/sections.txt -c copy -pix_fmt yuv420p -crf 18 ../videos/'" + song + "'" + currentTime + ".mp4";
    //joinVideoCommand = "ffmpeg -y -f concat -safe 0 -i - ../videos/sections.txt -c copy ../videos/'" + song + "'" + currentTime + ".mp4";
    std::thread startUp(clearSections);
    startUp.join();

    if(record == 1.0){ playSong = 0.0; } // Can't listen to the song when recording

    // Change start time based on settings
    // We change the play time of the MP3
    // further down the line
    playbackSpeed = std::stof(playbackSpeedString);
    timeOffset -= (std::stof(startTime)) * (1./playbackSpeed);


    // Read all the things we need from files
    std::vector<float> animData = getAnimData(song, version);
    std::vector<float> colourData = getColourData(song, version);
    std::vector<std::string> images = getImages(imageFile);
    images.insert(images.begin(), "../data/transparent.png");
    int activeTexture = 0;
    int totalFrames = animData.size()/122 + 60;

    // Setup engine to play song
    // We actually start playing later for sync issues
    irrklang::ISoundEngine *SoundEngine = irrklang::createIrrKlangDevice();

    // Init glfw
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    // Create window
    GLFWwindow* window = NULL;
    GLFWmonitor* primary = NULL;
    int w = SCR_WIDTH;
    int h = SCR_HEIGHT;
    if(fullscreen == 1.0)
    {
      primary = glfwGetPrimaryMonitor();
      const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
      w = mode->width;
      h = mode->height;
    }
    window = glfwCreateWindow(w, h, "Music Visualiser", primary, NULL);


    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);


    // Load OpenGL
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }


    // This shader code just passes the texture
    // infomation onto the main fragment shaders
    const char *vShaderCode = "#version 330 core\n"
                              "layout (location = 0) in vec3 aPos;\n"
                              "layout (location = 1) in vec3 aColor;\n"
                              "layout (location = 2) in vec2 aTexCoord;\n"
                              "out vec3 ourColor;\n"
                              "out vec2 TexCoord;\n"
                              "void main()\n"
                              "{\n"
                              "gl_Position = vec4(aPos, 1.0);\n"
                              "ourColor = aColor;\n"
                              "TexCoord = vec2(aTexCoord.x, aTexCoord.y);\n"
                              "}\0";
    unsigned int vertex;
    // vertex shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);

    // Fragment Shader
    unsigned int fragmentShader;
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);


    // Shader program
    unsigned int shaderProgram;
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertex);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);
    glDeleteShader(fragmentShader);

    // Shows error messages from shader compilation
    int success;
    char infoLog[512];
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

     // A rectangle which displays the active texture
     float textureVerticies[] = {
         // positions          // colors           // texture coords
          1.f,  1.f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
          1.f, -1.f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
         -1.f, -1.f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
         -1.f,  1.f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left
     };
     // Order which we render the points
     unsigned int indices[] = { 0, 1, 3, 1, 2, 3 };



     unsigned int VBO_texture, VAO_texture, EBO_texture;
     glGenVertexArrays(1, &VAO_texture);
     glGenBuffers(1, &VBO_texture);
     glGenBuffers(1, &EBO_texture);

     glBindVertexArray(VAO_texture);

     glBindBuffer(GL_ARRAY_BUFFER, VBO_texture);
     glBufferData(GL_ARRAY_BUFFER, sizeof(textureVerticies), textureVerticies, GL_STATIC_DRAW);

     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_texture);
     glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

     // position attribute
     glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
     glEnableVertexAttribArray(0);
     // color attribute
     glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
     glEnableVertexAttribArray(1);
     // texture coord attribute
     glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
     glEnableVertexAttribArray(2);


    // Textures
    int numImages = images.size();
    int widths[numImages];
    int heights[numImages];
    int nrChannelss[numImages];
    unsigned int textures[numImages];
    unsigned char* datas[numImages];


    stbi_set_flip_vertically_on_load(true);
    stbi_flip_vertically_on_write(true);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);


    for(int i = 0; i < numImages; i++)//"../data/" + song + "/images/" + images[i]
    {
        std::cout << "Loading: " << (images[i]).c_str() << std::endl;
        datas[i] = stbi_load((images[i]).c_str(), &widths[i], &heights[i], &nrChannelss[i], 0);
        glGenTextures(1, &textures[i]);
        glBindTexture(GL_TEXTURE_2D, textures[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, widths[i], heights[i], 0, GL_RGBA, GL_UNSIGNED_BYTE, datas[i]);
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    // This is the setup for passing data
    // back and forth
    float timeValue = glfwGetTime();
    int screenUniformLoc = glGetUniformLocation(shaderProgram, "screen");
    int rayMarchingUniformLoc = glGetUniformLocation(shaderProgram, "rayMarchingSteps");
    glUniform1f(rayMarchingUniformLoc, rayMarchingSteps);
    int timeUniformLoc = glGetUniformLocation(shaderProgram, "time");
    int colourUniformLoc = glGetUniformLocation(shaderProgram, "colours");

    // All the variables that need to be passed to the shader
    int fxScreenUniformLoc = glGetUniformLocation(shaderProgram, "effectScreen");
    int fxBackUniformLoc = glGetUniformLocation(shaderProgram, "effectBackG");
    int fx3DUniformLoc = glGetUniformLocation(shaderProgram, "effect3D");
    int fxFilterUniformLoc = glGetUniformLocation(shaderProgram, "effectFilter");


    // Piping data to their indivdual arrays
    float effectScreen[30];
    float effectBackG[30];
    float effect3D[30];
    float effectFilter[30];
    float lineLength = 121;
    float* coloursPointer = &colourData[0];


    memcpy(effectScreen, &animData[0], sizeof(float) * 30);
    memcpy(effectBackG, &animData[30], sizeof(float) * 30);
    memcpy(effect3D, &animData[60], sizeof(float) * 30);
    memcpy(effectFilter, &animData[120], sizeof(float) * 30);


    // Assigning all the pointers
    float *screenPointer = effectScreen;
    float *backGPointer = effectBackG;
    float *_3DPointer = effect3D;
    float *filterPointer = effectFilter;

    // Start Playing Song
    bool stillPlayingSong = 1;
    irrklang::ISound* playingSong = SoundEngine->play2D(mp3File.c_str(), GL_FALSE, false, true);
    if(playSong == 1.0)
    {
      // Set Playback time to where it should be
      playingSong->setPlayPosition((abs(timeOffset - sync)*1000.)*playbackSpeed);
      playingSong->setPlaybackSpeed (playbackSpeed);
    }
    else
    {
      playingSong->setIsPaused(true);
    }



    int index = 0;
    int varNum = 30;

    float frame = 0;

    std::string frameString = std::to_string((int)frame);
    std::string imagesMergeCommand = "cat ../videos/frame*.png | ffmpeg -y -f image2pipe -i - ../videos/section" + frameString + ".mp4";
    std::stringstream ss;
    // render loop
    // i.e keep this loop going while the song is still playing, or in the case we are recoring,
    // while there's frames to go
    while (!glfwWindowShouldClose(window) && ((stillPlayingSong && record == 0.0) || (record == 1.0 && (frame < totalFrames))))
    {
        // update
        if(record == 1.0)
        {
          timeValue = (1. / 60.) * frame;
          ss.str("");
          ss.clear();
          // Give all the frames a  unique name, ffmpeg breaks with frame numbers < 100 so 100 is added
          ss << std::setw(6) << std::setfill('0') << (int)frame + 100;
          frameString = ss.str();

          imagesMergeCommand = "cat ../videos/frame*.png | ffmpeg -r " + framerate + " -i - -c:v libx264 -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\"  -crf 18 -framerate " + framerate + " -pix_fmt yuv420p ../videos/section" + frameString + ".mp4";
          saveScreenshot(("../videos/frame" + frameString + ".png").c_str());
          if((int)frame % 60 == 0 && frame > 0){ std::thread addImagesThread(imagesToVideoSegment, imagesMergeCommand, 1, frameString); addImagesThread.join(); }

        }
        else
        {
          timeValue = (glfwGetTime() - timeOffset) * playbackSpeed - wait;
          timeValue = (round(timeValue*60.))/60.; // Nearest frame
        }

        // wait for the wait
        if(timeValue < 0)
        {
          playingSong->setPlayPosition((abs(timeOffset - sync)*1000.)*playbackSpeed);
        }

        // input
        processInput(window);


        if(timeValue < 0) timeValue = .0;
        glfwGetWindowSize(window, &SCR_WIDTH, &SCR_HEIGHT);

        // Clear
        clearScreen();

        // Draw Plain Box on the screen
        // To start ray marching on
        glBindTexture(GL_TEXTURE_2D, textures[activeTexture]);
        glUseProgram(shaderProgram);

        glBindVertexArray(VAO_texture);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        glUniform1f(timeUniformLoc, timeValue);
        glUniform2f(screenUniformLoc, SCR_WIDTH, SCR_HEIGHT);

        // Update variables from text file
        index = std::floor((timeValue*1000.) / 16.6666666) * lineLength;
        memcpy(effectScreen, &animData[index], sizeof(float) * 30);
        memcpy(effectBackG, &animData[index + 30], sizeof(float) * 30);
        memcpy(effect3D, &animData[index + 60], sizeof(float) * 30);
        memcpy(effectFilter, &animData[index + 90], sizeof(float) * 30);
        activeTexture = (int)(animData[index + 120]);

        glUniform1fv(fxScreenUniformLoc, varNum, screenPointer);
        glUniform1fv(fxBackUniformLoc, varNum, backGPointer);
        glUniform1fv(fx3DUniformLoc, varNum, _3DPointer);
        glUniform1fv(fxFilterUniformLoc, varNum, filterPointer);
        glUniform1fv(colourUniformLoc, 15, coloursPointer);

        // Printout active functions and the time value
        std::cout << std::setw(8) << frame << "/" << totalFrames;
        for(int i = 0; i < lineLength - 1; i+=10)
        {
          std::cout <<  std::setw(3) << animData[index + i] << " ";
        }
        std::cout << animData[index + 120] << std::endl;


        // Check the songs still playing
        // but if we didn't start it in the first place don't check
        if(playSong == 1.0){ stillPlayingSong = SoundEngine->isCurrentlyPlaying(mp3File.c_str()); }

        // increment frames
        frame += 1;
        if(record == 1.0 && framerate == "30.0"){ frame += 1; } // 30 fps recordings


        // Poll IO and update
        glfwSwapBuffers(window);
        glfwPollEvents();


    }


    // clean up
    glfwTerminate();
    //recordThread.join();
    if(record == 1.0)
    {
        std::cout << audioCommand << std::endl;
        std::thread addImagesThread(imagesToVideoSegment, imagesMergeCommand, 1, frameString);
        addImagesThread.join();
        std::thread audioThread(saveVideo, joinVideoCommand, audioCommand, record);
        audioThread.join();
    }

    return 0;
}
