"""
This file geneartes all the metadata
after reading from MIDI files.
It works when the files are split up on a per instrument
basis, or if there is one MIDI file for the whole track.

For every track.
We find out

Has a note been struck, note velocity, note pitch, note duration

As well as this we link the FFT data on the whole song.
Where we track the volume of the song over the duration,
as well as a colour, which is determined by the frequency distribution.

We also look at the song settings file to see what type of falloff,
we want for each of the parameters. And apply that to the track
"""

import sys, os, pickle, time, copy, random, json
from mido import MidiFile
from mido import tick2second
from mido import Message
from mido import MidiTrack
from mido import tempo2bpm
from enum import Enum


midiInstruments = ["Piano", "ChromaticPercussion", "Organ", "Guitar",
                   "Bass", "Strings", "Ensemble", "Brass", "Reed",
                   "Pipe", "SynthLead", "SynthPad", "SynthEffects",
                   "Ethnic", "Percussive", "SoundEffects", "Drums",
                   "Kick", "Snare", "Cymbals"]

# All the instruments in each song
# Keeping it simple for now
parts = {}

ticksPerBeat = 0 # used to go from MIDI time -> real time
tempo = 0
bpm = 0
songLength = 0
msPerBeat = 0

def getInstName(fileName):
    """ Get instrument from the file name """
    # Drums2.mid --> Drums2
    # Bass.mid   --> Bass
    fileName = fileName.split(".")[0]
    return fileName


def getAllFiles(data, song):
    """ Get all the MIDI files from the current directory """
    allFiles = []
    for file in os.listdir(data):
        if file.endswith(".mid"):
            allFiles.append(file)
    if(len(allFiles) > 1):
        if(song + ".mid" in allFiles):
            allFiles.remove(song + ".mid")
    return allFiles


def getFalloffList(n):
    """" Works out a hardcoded list of multiplication values to create a nice falloff"""
    if(n < 1):
        n = 1
    k = 0.99
    step = 0.01
    s = 1.0

    while k > 0:
        for i in range(n):
            s = s * k
        if(s < 0.01):
            break
        else:
            s = 1.0
            k -= step

    multiplier = []
    s = 1.0
    for i in range(n):
        s = s * k
        multiplier.append(s)
    return(multiplier)

def getFalloffAmounts(structure):
    """ We need to find, for duration, pitch, velocity
        what the falloff amounts are, and calculate them """
    if(os.path.isfile(structure)):
        with open(structure, "r") as f:
            structure = json.load(f)
        noteFalloff = int(structure["settings"]["pitchFalloff"])
        durationFalloff = int(structure["settings"]["durationFalloff"])
        velocityFalloff = int(structure["settings"]["velocityFalloff"])
    else:
        noteFalloff = 50
        durationFalloff = 50
        velocityFalloff = 50

    falloff = {}
    falloff["note"] = getFalloffList(noteFalloff)
    falloff["duration"] = getFalloffList(durationFalloff)
    falloff["velocity"] = getFalloffList(velocityFalloff)
    return falloff

def getSeconds(ticks):
    """ Convert from MIDI ticks to milliseconds """
    return(int(round(tick2second(ticks, ticksPerBeat, tempo)*1000)))

def parseNotes(section, inst):
    """ Converts MIDI representation of notes to the way we need """
    notes = {"start": [], "note": [], "velocity": [], "duration": []}


    time = 0

    for i in range(len(section)):
        msg = section[i]

        # There's loads of messages that carry time infomation. But we only
        # care about a few
        if(msg.type in ["note_on", "note_off", "control_change", "pitchwheel", "sysex", "program_change", "set_tempo", "time_signature", "track_name", "end_of_track"]):
            time += msg.time

        if(msg.type == "note_on"):
            noteStart = getSeconds(time)
            note = msg.note
            vel = msg.velocity
            dur = 0

            # Look for the end of the note
            for j in range(i+1,len(section)):
                msg2 = section[j]
                dur += msg2.time
                # was a bug where some MIDI notes would be turned on twice
                # before they are turned off, we may get some issues with this
                if((msg2.type == "note_off" or msg2.type == "note_on") and msg2.note == note):
                    notes["start"].append(noteStart)
                    notes["note"].append(note)
                    notes["velocity"].append(vel)

                    # All the drum tracks made in a single midi track have drum
                    # durations too small to see in a visualisation so need to be
                    # given a min lenght
                    noteDuration = float(getSeconds(dur))
                    if(noteDuration < 100 and inst in ["Drums", "Snare", "Kick", "Cymbals"]):
                        noteDuration = 100
                    notes["duration"].append(noteDuration)
                    break
    return notes


def resolveInstrument(channels, num, returnIndex=False):
    """ From the MIDI number find the instrument type
        This is a great function. Very nicely written (I think) """
    # These are the delimiting values for the MIDI instrument families
    lowerRange = [-1, 7, 15, 23, 31, 39, 47, 55, 63, 71, 79, 87, 95, 103, 111, 119]
    upperRange = [8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 120, 128]
    index = 0

    # Drums are always 9 in MIDI standard
    if(num == 9):
        index = 16
    else:
        instNumber = channels[num]
        for i in range(len(lowerRange)):
            if(instNumber > lowerRange[i] and instNumber < upperRange[i]):
                break
            index += 1

    # Nice option to only get the index if that's all we care about
    if(returnIndex):
        return index
    else:
        return midiInstruments[index]

def resolveDrum(num):
    """ From the GM1 Soundset specification, we must find if
        the currently playing drum is a bass, snare, or cymbal """
    if(num in [35, 36]):  # Bass drum sounds
        return 1
    elif(num in [37, 38, 39, 40]): # Snare drum sounds
        return 2
    elif(num in [42, 44, 46, 49, 51, 52, 53, 55, 56, 57, 59, 70]): # Cymbals
        return 3
    else:
        return 0 # Still a drum, but we don't care about it


def splitTracks(messages, data):
    """ This is only done when we have a single MIDI track,
        as there are two different formats. This is for the ones
        I got online, where everything is in one big track """
    i = 0

    # MIDI Channels work wierdly, drums have their own seperate channel
    # to account for. So there isn't actually any more channels.
    # So there are 16 instrument families. And a sperate one for drums.
    # But only 16 channels. Bceause of the channel mappings.
    # Channel 10 is always set to 0 when we start palying drums

    # As well as this. We also create 3 seperate channels for the
    # kick, snare and cymbals. So you can just follow one of those

    numChannels = 20 # 16 + Extra one for drums + extra 3 for seperated drums
    instFamCount  = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    channels      = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    # We also need a list of drum duplicate messages
    # So we know when to add them. As they will be the same messages
    # as the main kit. So can't edit them. But they need different timing
    # delays to account for the delta time
    duplicateDrumMessages = [[], [], []]
    duplicateDrumIndexes = [[], [], []]

    # When splitting, some channels will miss out messages.
    # To keep the timings right, we need to add on what they've missed
    # So they are stored here
    timeOffsets = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


    # Since we are splitting the MIDI files up, we need to know which ones are
    # Going be in each file. All meta messages and things like that must
    # go in all of them, we just don't care about messages from other channels
    # So all channel specific messages get split up into here.
    lines = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]

    for msg in messages:

        if(msg.type == "program_change"):
            channels[msg.channel] = msg.program

        if(msg.type == "note_on" or msg.type == "note_off"):

            # Find out what instrument family the current note is in
            thisInst = resolveInstrument(channels, msg.channel, True)

            if(thisInst == 16): # a drum
                typeOfDrum = resolveDrum(msg.note)
                if(typeOfDrum > 0): # a valid drum to put in it's own track

                    # Update all the values for this sub channel drum
                    # make sure to reset for the main drum
                    # we have to make a copy of the message here
                    duplicateMessage = copy.deepcopy(msg)
                    instFamCount[thisInst + typeOfDrum] += 1
                    duplicateMessage.time += timeOffsets[thisInst + typeOfDrum]
                    duplicateDrumMessages[typeOfDrum - 1].append(duplicateMessage)
                    duplicateDrumIndexes[typeOfDrum - 1].append(i)
                    lines[thisInst + typeOfDrum].append(duplicateMessage)
                    timeOffsets[thisInst + typeOfDrum] = 0


            instFamCount[thisInst] += 1       # Increment how many notes this inst has played

            # Let every other channel have their timing updated
            # So they're not out of sync when they play
            for j in range(numChannels):
                if((thisInst == 16 and j != thisInst + typeOfDrum) or thisInst != 16): # Drums shouldn't increment the time of other drums
                    if(j != thisInst):
                        timeOffsets[j] += msg.time

            msg.time += timeOffsets[thisInst] # If the instrument hasn't been played in a while, update this time value
            timeOffsets[thisInst] = 0          # Because now it has caught up with everyone else
            lines[thisInst].append(msg)       # Add to the lines it must keep
        i += 1



    for i in range(len(lines)):
        if(len(lines[i]) > 0):

            if(i > 16):
                # It's a duplicate drum
                # So we replace messages with relevent ones to duplicated drums
                # unfortunately more deep copy's are required
                changedDrumMessages = copy.deepcopy(messages)
                for j in range(len(duplicateDrumIndexes[i - 17])):
                    changedDrumMessages[duplicateDrumIndexes[i - 17][j]] = duplicateDrumMessages[i - 17][j]
                saveTrack(i, lines[i], changedDrumMessages, data)

            # Can just save with the normal notes
            else:
                saveTrack(i, lines[i], messages, data)
    print("Tracks are now saved individually")
    print("The program will have to be ran again to generate the metadata")


def saveTrack(instrument, notes, messages, data):
    """ Now we have all the correct data, we save them individually """

    thisInst = midiInstruments[instrument]
    mid = MidiFile(ticks_per_beat = ticksPerBeat)
    track = MidiTrack()
    mid.tracks.append(track)

    stdNotesSaved = 0
    uniqueNotesSaved = 0

    for msg in messages:
        if(msg.type == "note_on" or msg.type == "note_off"):
            # If we don't have a note for the instrument. Just add an empty message
            if(msg in notes):
                track.append(msg)
                uniqueNotesSaved += 1

        else:
            track.append(msg)
            stdNotesSaved += 1

    mid.save(data + "/" + thisInst + ".mid")



def normalize(a):
    """ Normalizes a list, between the range of .2 and 1.0"""
    m = max(a)
    mi = min(a)
    low = 0.2
    if(m != mi):
        # Look at the size of that line
        return [round(((float(i) - mi) / (m - mi))*(1 - low) + low, 3) for i in a]
    else:
        return([1. for i in a])


def normalizeData():
    """ Normalizes the note pitch and note velocity
        to work better with the graphical functions """
    instruments = list(parts.keys())
    for i in instruments:
        parts[i]["note"] = normalize(parts[i]["note"])
        parts[i]["velocity"] = normalize(parts[i]["velocity"])

def generateInstrumentFrameData(instName, falloff):
    """ Once we know when all the events are happeneing,
        we need a 60fps list, at each frame (row) we need,
        to know the status of all the variables at each frame """

    # May be a problem here where if two notes are played at
    # the exact same time we take any value when we should be taking
    # the one with the longest duration
    thisFrameData = []
    thisInst = parts[instName]
    cTime = 0
    noteCount = 0
    frame = 1000. / 60.

    activeNotes = []
    lastPlayedNote = -1
    cNote = 0.0


    # Had to do some extra calculating
    while(noteCount < len(thisInst["start"]) or (noteCount == len(thisInst["start"]) and len(activeNotes) > 0)):


        # If any notes begin, add them to the active
        if(noteCount < len(thisInst["start"])):
            if(thisInst["start"][noteCount] <= cTime):
                cNote = 1.0 # Only happends when a note is struck
                activeNotes.append(noteCount)
                noteCount += 1

        # If any active ones are too old, remove them
        toDelete = []
        for n in activeNotes:
            if(cTime > (thisInst["start"][n] + thisInst["duration"][n])):
                toDelete.append(n)
        for d in toDelete:
            activeNotes.remove(d)
            lastPlayedNote = d

        # No active notes, do 0
        if(len(activeNotes) == 0):
            if(lastPlayedNote == -1):
                thisFrameData.append([0., 0., 0., 0. ])
            else:
                # This falloff is adjustable in the metadata file
                # and allows smoother colour transitions
                index = cTime - (thisInst["start"][lastPlayedNote] + thisInst["duration"][lastPlayedNote])
                index = int(index // 16.6666666)   # To go from ms to frames
                falloffValues = [0.0, 0.0, 0.0]
                variables = ["note", "velocity", "duration"]

                for i in range(len(variables)):
                    if(index < len(falloff[variables[i]])):
                        falloffValues[i] = round((thisInst[variables[i]][lastPlayedNote] * falloff[variables[i]][index]), 3)


                # Add all the falloffs to the data
                thisFrameData.append([0., falloffValues[0],
                                      falloffValues[1], falloffValues[2]])

        # Else, do the most recent notes data
        else:
            r = activeNotes[-1]

            # Gives a smooth transition between notes
            if(thisInst["duration"][r] > 0):
                lerpCNote = 1 - (cTime - thisInst["start"][r]) / thisInst["duration"][r]
            else:
                lerpCNote = 0

            if(lerpCNote > 1):
                lerpCNote = 1
            elif(lerpCNote < 0):
                lerpCNote = 0
            lerpCNote = round(lerpCNote, 3)
            thisFrameData.append([lerpCNote, thisInst["note"][r],
                                  thisInst["velocity"][r], thisInst["duration"][r]])
        # Next frame
        cNote = 0.0
        cTime += frame

    return thisFrameData

def generateFrameData(falloff, metadata):
    """ Goes through each instrument and gets all the data we need for each frame """
    instruments = sorted(list(parts.keys()))
    frameData = {}

    for instrument in instruments:
        frameData[instrument] = generateInstrumentFrameData(instrument, falloff)

    longest = max([len(frameData[i]) for i in instruments])

    for instrument in instruments:
        for i in range(longest - len(frameData[instrument])):
            frameData[instrument].append([.0, .0, .0 , .0])

    line = 0
    file = open(metadata, "w")

    # Then write all the real data
    while(line < len(frameData[instruments[0]]) - 1):
        thisLine = [frameData[instrument][line] for instrument in instruments]
        thisLine = [i for j in thisLine for i in j]
        thisLine = (" ").join(" " + str(i) for i in thisLine)
        thisLine += "\n"
        file.write(thisLine)
        line += 1
    file.close()
    return

def randCol():
    return [random.randint(1,255), random.randint(1,255), random.randint(1,255)]

def generateStructData(structure):
    """ Makes an outline of a struct data that all the new ones are copied off """
    if not os.path.exists(structure):
        structureData = {}
        structureData["info"] = {}
        structureData["info"]["bpm"] = int(bpm)
        structureData["info"]["msPerBeat"] = int(msPerBeat)
        structureData["info"]["songLength"] = int(round(songLength * 1000))

        structureData["settings"] = {}
        structureData["settings"]["durationFalloff"] = 30
        structureData["settings"]["pitchFalloff"] = 30
        structureData["settings"]["velocityFalloff"] = 30

        structureData["colours"] = []

        for i in range(5):
            structureData["colours"].append(randCol())

        structureData["effects"] = {}

        with open(structure, "w") as f:
            json.dump(structureData, f, indent = 2)

def generateImageData(imagePath):
    """ Sets up an initial photos folder to put things in """
    if not os.path.exists(imagePath):
        os.makedirs(imagePath)
    if not os.path.exists(imagePath + "/allImages.txt"):
        with open(imagePath + "/allImages.txt","w+") as f:
            pass # Just create the file

def timeOfSection(section):
    # This is to fix the timings from breaks in sections
    time = 0
    for i in range(len(section)):
        msg = section[i]
        if(msg.type in ["note_on", "note_off", "control_change", "pitchwheel", "sysex", "program_change", "set_tempo", "time_signature", "track_name", "end_of_track"]):
            time += msg.time
    return time


def generateMetaData(data, song):
    """ Finds every MIDI file and puts it into one metadata file for the song """
    global ticksPerBeat, tempo, songLength, bpm, msPerBeat

    splitUpFile = False
    tempo = 0
    # There is only one midi file, so we need to split it
    if(len(getAllFiles(data, song)) == 1):
        print("Splitting up main MIDI file into seperate ones")
        splitUpFile = True

    # The file is in the format we like
    for f in getAllFiles(data, song):
        # Don't do midi files with the same title
        parts[getInstName(f)] = []
        instrumentParts = []
        mid = MidiFile(os.path.join(data, f))
        ticksPerBeat = mid.ticks_per_beat


        # We need to put all the sections together into one big section
        # here or else we have horrible dictionary unpacking issues
        fullTrack = []
        lenPriorSection = 0


        for i, section in enumerate(mid.tracks):
            if(splitUpFile == True):
                splitTracks(section, data)
                return 1 # Needs to be ran again

            # Adjust the timing of the first note to fix
            # timing errors casued by sections
            for msg in section:
                if(msg.type == "note_on" or msg.type == "note_off" or msg.type == "control_change"):
                    msg.time -= lenPriorSection
                    break
            fullTrack = fullTrack + section
            lenPriorSection += timeOfSection(section)

        # Set the tempo only once
        if(tempo == 0):
            for k in range(len(fullTrack)):
                if(fullTrack[k].type == "set_tempo"):
                    # Get all these globals sorted out
                    tempo = fullTrack[k].tempo
                    bpm = tempo2bpm(tempo)
                    songLength = mid.length
                    msPerBeat = getSeconds(ticksPerBeat)
                    print("MS per beat: ", int(getSeconds(ticksPerBeat)))
                    print("BPM: ", int(tempo2bpm(tempo)))
                    print("Length: ", int(mid.length))
                    break

        thisTrack = parseNotes(fullTrack, getInstName(f))
        # Convert MIDI notes to the format we want and
        # put them in the dictionary
        if(len(thisTrack) > 0):
            parts[getInstName(f)] = thisTrack

    return 0 # does not need to be ran again

def getAllSongs(directory):
    """ Gets every song in data, and every song verison in that """
    structFiles = []
    songs = [x[1] for x in os.walk(directory)][0]
    return songs

def generateAllMetadata(directory):
    """ Goes through every file and generates all the MIDI, reports any errors"""
    global parts
    toReturn = ""
    toRunAgain = []
    for song in getAllSongs(directory):
        print("\nSong: ", song)
        parts = {}
        data = directory + song
        metadata = directory + song + "/metadata.txt"
        structure = directory + song + "/structureData.json"
        images = directory + song + "/images"

        falloff = getFalloffAmounts(structure)
        if(generateMetaData(data, song) == 0): # ran normallly
            normalizeData()
            generateFrameData(falloff, metadata)
            generateStructData(structure)
            generateImageData(images)
            toReturn += song + " ran successfully\n"
        else:
            toReturn += song + " must be ran again\n"

    return toReturn


def main():
    """ Takes into account the program can be ran from outside
        if no arguments are provided will do the MIDI for every song """
    # Let's the program be ran from elsewhere
    directory = "../data/"

    if(os.getcwd().endswith("fractals")):
        directory = directory[3:]

    if(len(sys.argv) == 2 and sys.argv[1] != "run"):
        song = sys.argv[1] # Put a Song Name Here

        data = directory + song
        metadata = directory + song + "/metadata.txt"
        structure = directory + song + "/structureData.json"
        images = directory + song + "/images"

        falloff = getFalloffAmounts(structure)

        if(generateMetaData(data, song) == 0):
            normalizeData()
            generateFrameData(falloff, metadata)
            generateStructData(structure)
            generateImageData(images)

    else:
        return(generateAllMetadata(directory))


if __name__ == "__main__":
    main()
