"""
Shows the animation data for any .wav
file
"""

import fftData
import time

import matplotlib.pyplot as plt
import numpy as np
# Setup figure, axis and initiate plot
plt.ion() # Stop matplotlib windows from blocking
fig, ax = plt.subplots()
xdata, ydata = [], []
ln, = ax.plot([], [], 'b')

# Since it's the bass guitar only show the first harmonics
ax.set_xbound(0, .5)
ax.set_ybound(-80, 0)

cTime = 0
samplesPerSecond = 60
timestep = 1 / samplesPerSecond
calcDuration = 0
xdata = []
ydata = []


outfile = open("BassOut.txt", "w")

# Load in the data for the bass guitar to animate it
fileName = input("Please enter file name, leave blank for Bass.wav: ")
if(fileName == ""):
    fileName = "SongTest/Bass.wav" # Default

fileFFT = fftData.FFTData(fileName, "")

while True:
    time.sleep(max(timestep - calcDuration, 0))
    timerStart = time.time()
    cTime += timestep

    xdata, ydata = fileFFT.getFFTData(cTime, cTime + timestep)

    maxValue = xdata[np.where(np.isclose(ydata, max(ydata)))] * 1000
    print(round(cTime, 2), "\t", maxValue, "\t", max(ydata), "\t", round(sum(ydata), 2))
    outfile.write(str(maxValue[0]) + "," + str(max(ydata)))
    outfile.write("\n")

    # Reset the data in the plot
    ln.set_xdata(xdata)
    ln.set_ydata(ydata)

    # Update the window
    fig.canvas.draw()
    fig.canvas.flush_events()
    calcDuration = time.time() - timerStart

outfile.close()
