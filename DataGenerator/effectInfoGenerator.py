"""
This program
trawls thogh my CPP shader code
and finds out all the deatils about them and
stores them in JSON
"""
import json, os


def readAndSplitFile(f):
    rawEffects = []

    # Read
    f = open(f, "r")
    shader = f.read().split("\n")
    f.close()

    # Split by custom notation
    reading = False
    backgroundEffects = []
    for l in shader:
        if(l == "//---"):
            reading = not reading
        if(reading):
            rawEffects.append(l)

    # Split effects, remove comments and split
    rawEffects = ("".join(rawEffects)).split("//---")[1:]
    for i in range(len(rawEffects)):
        rawEffects[i] = rawEffects[i].split("//")[1:]
        rawEffects[i] = [j.strip() for j in rawEffects[i]]
    return rawEffects

def putInDict(effects):
    toReturn = []
    for fx in effects:
        effect = {}
        effect["title"] = fx[0]
        effect["description"] = []
        effect["args"] = []
        for i in range(len(fx) -1, -1, -1):
            if(":" in fx[i]):
                effect["args"].append(fx[i])
            else:
                break
        if(i > 1):
            effect["description"] = fx[1:i+1]
        effect["args"].reverse()
        toReturn.append(effect)
    return toReturn

def updateEffectInfo():
    directory = "../3dRayMarching/shaders/"
    if(os.getcwd().endswith("fractals")):
        directory = directory[3:]

    screenEffects = directory + "screenEffects.c"
    backgroundEffects = directory + "backgroundEffects.c"
    _3DEffects = directory + "3DEffects.c"
    filterEffects = directory + "filterEffects.c"

    effectDescriptions = {}
    effectDescriptions["screenEffects"] = putInDict(readAndSplitFile(screenEffects))
    effectDescriptions["backgroundEffects"] = putInDict(readAndSplitFile(backgroundEffects))
    effectDescriptions["Effects3D"] = putInDict(readAndSplitFile(_3DEffects))
    effectDescriptions["filterEffects"] = putInDict(readAndSplitFile(filterEffects))

    saveLoc = "effectInfo.json"
    if(os.getcwd().endswith("fractals")):
        saveLoc = "DataGenerator/" + saveLoc
    with open(saveLoc, "w") as fp:
        if(os.getcwd().endswith("fractals")):
            json.dump(effectDescriptions, fp, indent = 2)
    return("Effect Infomation Has Been Updated")

if __name__ == "__main__":
    updateEffectInfo()
