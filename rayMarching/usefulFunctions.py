import math
from collections import namedtuple
"""
This file contains things useful for
rotating shapes and some of the maths.
This stuff should be able to be re used when
I scale this up to 3D
"""

def clamp(a, minimum = 0, maximum = 1):
    """ Clamp takes a number and makes it stay
        between the range that we want         """

    if(a < minimum):
        a = minimum
    elif(a > maximum):
        a = maximum
    return a

def dot(a, b):
    """ This carries out the dot product
        on two vectors, and returns the vector """
    out = []
    if(len(a) == len(b)):
        for i in range(len(a)):
            out.append(float(a[i] * b[i]))
        return out
    else:
        return None

def dot_value(a ,b):
    """ This returns the value of a dot product """
    out = 0
    if(len(a) == len(b)):
        for i in range(len(a)):
            out += a[i] * b[i]
        return float(out)
    else:
        return None

def rotate_point(px, py, rx, ry, angle, degrees = False):
    """ This function takes a point     px, py
        and rotates it about point      rx, rx
        with angle                      angle

        the angle is given in radians becauase it's more native
        and lends itself to some of the other functions better
    """

    if(degrees):
        angle = math.radians(angle) # if user wants to provide degrees
    # the sin and cos componenets
    s = math.sin(angle)
    c = math.cos(angle)
    # rotate about origin
    px -= rx
    py -= ry
    # rotate point
    px2 = px * c - py * s
    py2 = px * s + py * c
    # put it back
    px = px2 + rx
    py = py2 + ry

    return px,py

def rotate_polygon(points, cx, cy, angle, degrees = False):
    """ This rotates a sequence of points. So if we have,
        some shape we are drawing, we can rotate and still draw
    """
    for i in range(len(points)):
        points[i] = rotate_point(points[i][0], points[i][1], cx, cy, angle, degrees)

    return points
