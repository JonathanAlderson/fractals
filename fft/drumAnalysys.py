"""
Does the drum analysis section of the FFT
The first 4 options show how the FFT graphs vary
The last one shows how good FFT is at syncing data
from audio for use in the final program.
"""
import numpy as np
import time
import math
import sys
import matplotlib.pyplot as plt
import pylab
from scipy.io import wavfile
from scipy.fftpack import fft
import matplotlib.pyplot as plt
import pygame
import threading

class FFT:
    fullsample = None
    fileName = None
    samplingFreq = None
    songDuration = None

    def __init__(self, fileName):
        # Just load this one time
        self.fileName = fileName
        #Read file and get sampling freq [ usually 44100 Hz ]  and sound object
        self.samplingFreq, self.fullSample = wavfile.read(self.fileName)

        self.fullSample = self.fullSample / (2.**15)          # Normalize wav file data
        self.fullSample = self.fullSample[:,0]                # Get rid of other channel
        self.songDuration =  self.fullSample.shape[0] / self.samplingFreq

    def getFFTData(self, start, end):

        subSample = self.fullSample[int(start * self.samplingFreq): int(end * self.samplingFreq)]

        #Get length of mySound object array
        subSampleLength = len(subSample)
        #Take the Fourier transformation on given sample point
        fftArray = fft(subSample)
        numUniquePoints = int(np.ceil((subSampleLength + 1) / 2.0))
        fftArray = fftArray[0:numUniquePoints]
        fftArray = abs(fftArray)                     # Sorts out imaginary bits
        fftArray = fftArray / float(subSampleLength) # Sorts out magnitude
        fftArray = fftArray **2                      # Sorts out negatives

        # Multiply by 2
        if subSampleLength % 2 > 0: #we've got odd number of points in fft
            fftArray[1:len(fftArray)] = fftArray[1:len(fftArray)] * 2
        else:
            fftArray[1:len(fftArray) -1] = fftArray[1:len(fftArray) -1] * 2

        freqArray = np.arange(0, numUniquePoints, 1.0) * (self.samplingFreq / subSampleLength);
        return(freqArray/1000, 10 * np.log10(fftArray))

    def animateFFT(self):
        # Plotting
        plt.ion()
        fig, ax = plt.subplots()
        xdata, ydata = [], []
        ln, = ax.plot([], [], 'b')
        ax.set_xbound(0, 20)
        ax.set_ybound(-80, 0)
        cTime = 0
        samplesPerSecond = 60
        timestep = 1 / samplesPerSecond
        calcDuration = 0
        xdata = []
        ydata = []

        while True:
            # Timing
            time.sleep(max(timestep - calcDuration, 0))
            time.sleep(0.4)
            timerStart = time.time()
            cTime += timestep
            # Get data
            xdata, ydata = self.getFFTData(cTime, cTime + timestep)

            print(round(cTime, 2), "\t", max(ydata), "\t", round(sum(ydata), 2))

            # Reset the data in the plot
            ln.set_xdata(xdata)
            ln.set_ydata(ydata)

            # Update the window
            fig.canvas.draw()
            fig.canvas.flush_events()

            # Timing
            calcDuration = time.time() - timerStart

    def writeToFile(self, outFile=None):
        if(outFile == None):
            filename = self.filename[:-4] + "_fft_out.txt"
        outfile = open(filename, "w")
        cTime = 0
        samplesPerSecond = 60
        timestep = 1 / samplesPerSecond
        while True:
            xdata, ydata = func.getFFTData(cTime, cTime + timestep)
            maxValue = xdata[np.where(np.isclose(ydata, max(ydata)))] * 1000
            print(round(cTime, 2), "\t", maxValue, "\t", max(ydata), "\t", round(sum(ydata), 2))
            outfile.write(str(maxValue[0]) + "," + str(max(ydata)))
            outfile.write("\n")
            cTime += timestep
        outfile.close()

    def getMax(self, cutoff = 100):
        # This function is used by the drums to find
        # out what the outline for the bass, snare and hats look
        # like, so they can be more easily recognised
        # cutoff is the % of the frequency space it keeps up to
        cTime = 0
        samplesPerSecond = 60
        timestep = 1 / samplesPerSecond
        maxStep = []
        maxStepVal = -1 * float('inf')
        while(cTime + timestep < self.songDuration):
            xdata, ydata = self.getFFTData(cTime, cTime + timestep)
            # if there is a cutoff replace rest with zeros
            if(cutoff != 100):
                p =  math.floor(len(ydata) * (cutoff / 100) )
                ydata2 = list(ydata[0:p]) + [0 for i in range(len(ydata) - p)]


            thisStepVal = sum(ydata)


            if(thisStepVal > maxStepVal):
                maxStepVal = thisStepVal
                maxStep = ydata
            cTime += timestep
        return maxStep

    def closeMatch(self, sounds, thesholds):
        # Looks through the song to see how close of a match
        # the sounds are at each point

        cTime = 0
        samplesPerSecond = 60
        timestep = 1 / samplesPerSecond
        maxStep = []
        maxStepVal = -1 * float('inf')
        calcDuration = 0
        time.sleep(.2)
        while(cTime + timestep < self.songDuration):
            time.sleep(max(timestep - calcDuration + 0.001, 0))
            timerStart = time.time()
            xdata, ydata = self.getFFTData(cTime, cTime + timestep)

            # Compare to all the sounds
            for i in range(len(sounds)):
                print(compareSounds(sounds[i], ydata, thesholds[i]), end="\t\t")
            print("")
            cTime += timestep

            calcDuration = time.time() - timerStart

        return maxStep

def compareSounds(a, b, threshold):
    # we are looking for the sound a, inside sound b
    # Compares fourier analysis of them
    minVolume = -80
    cutoff = 0.1    # how many db either side it can be
    closeness = 0

    for i in range(len(a)):
        if(a[i] > minVolume):
            thiscloseness = max(-pow(((b[i] - a[i]) / cutoff), 2) + 1, 0)
            closeness += thiscloseness
            #    print(a[i]," inside ", b[i]," gives ", thiscloseness)
    similarity = round(closeness / len(a),3)
    if(similarity < threshold):
        similarity = 0
    return(similarity)

def playThreadedMusic(songToPlay):
    pygame.mixer.init()
    pygame.mixer.music.load(songToPlay)
    pygame.mixer.music.play()


#########################
# MAIN
#########################

opt = input("\n\n\n\n\n\n\n\n1. View 10KHz Sin wave\n2. Bass Drum\n3. Snare Drum\n4. Hi-Hat\n5. Whole Drums Test\n\n\n")
try:
    opt = int(opt)
except:
    print("Please enter an int")
    sys.exit()


# Get sin wave sound
sinWave = FFT("SongTest/sinWave.wav")

# Get the sounds of all the instruments
bassDrum =  FFT("DrumTest/BassDrum.wav")
bassDrumSound = bassDrum.getMax(1)    # We have a cutoff for the lowend bass
snareDrum =  FFT("DrumTest/SnareDrum.wav")
snareDrumSound = snareDrum.getMax()
hiHat =  FFT("DrumTest/HiHat.wav")
hiHatSound = hiHat.getMax()

if(opt == 1):
    sinWave.animateFFT()

elif(opt == 2):
    bassDrum.animateFFT()

elif(opt == 3):
    snareDrum.animateFFT()

elif(opt == 4):
    hiHat.animateFFT()

elif(opt == 5):
    opt = input("\n\n\n\nSelect Drum Beat from 1-6\n\n1. Soundcheck and simple linear beat\n2. All combinations of voicings\n3. Fast linear drum beat\n4. Slow stanadrd beat\n5. Fast drum beat\n6. Fast drum beat with triplet hi hat rhythms\n\n")
    try:
        opt = int(opt)
        opt = str(opt)
    except:
        print("Please enter an int")
        sys.exit()

    thisSong = "DrumTest/Beat" + opt + ".wav"
    fullDrums = FFT(thisSong)



    if(True):
        thread1 = threading.Thread(target = playThreadedMusic, args = [thisSong])
        thread1.start()

    # The activation amounts for each beat
    activationAmounts = [[0.007, 0.014, 0.005], [0.005, 0.007, 0.007], [0.007, 0.007, 0.003], [0.007, 0.007, 0.003], [0.007, 0.007, 0.003], [0.007, 0.007, 0.003]]

    fullDrums.closeMatch([bassDrumSound, snareDrumSound, hiHatSound], activationAmounts[int(opt)-1])
