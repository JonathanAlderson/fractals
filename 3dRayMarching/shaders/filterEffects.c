//---
// Fade from black
// Makes the whole screen start out black and gradually go back to normal
// Duration: How long the fade takes
//---
vec3 fadeFromBlack(vec3 inCol, float duration)
{
  return inCol * smoothrange(0., 1., duration*duration);
}


//---
// Texture Static
// Draws your texture to the screen
// X: X position
// Y: Y position
// Scale: Scale of image
// Repeats: 0 to not repeat the image, 1 to repeat the image
// Opactiy: Hard set the opacity of the image
//---
vec3 staticTexture(vec3 inCol, vec2 texOffset, float x, float y, float scale, float repeats, float opacity)
{
  scale = 1./scale;

  vec2 imagePos = vec2(((TexCoord.x-.5)*scale)+.5 - x, ((TexCoord.y-.5)*scale)+.5 - y);
  imagePos += texOffset;

  vec4 tex = vec4(0.);
  if(repeats == 0.0)
  {
    if(imagePos.x > 0. && imagePos.x < 1. && imagePos.y > 0. && imagePos.y < 1.)
    {
      tex = texture(ourTexture, imagePos);
    }
  }
  else
  {
    tex = texture(ourTexture, imagePos);
  }

  // Texture overlay
  if(tex == vec4(0.)){ return inCol; }
  else{ return mix(inCol, tex.xyz, opacity); }
}


//---
// Texture Warp Tunnel
// Puts textures through a warp tunnel
// Effect: 0. Looping 1. Light Tunnel 2. Alien Light Tunnel 3. Zoom Out 4. Zoom In
// Speed: The movement speed of the textures
//---
vec3 warpTunnel(vec3 inCol, vec2 uv, vec2 texOffset, float dur, float effect, float speed)
{
  speed = smoothrange(1.0, 10.0, speed);
  float r2 = dot(uv, uv);
  float r = sqrt(r2);

  float a = atan(uv.y, uv.x);
  if(effect == 1.0){ a = cos(uv.y)*speed + cos(uv.x)*speed;}
  if(effect == 2.0){ a = sin(uv.y)*speed + cos(uv.x)*speed;}


  // make the tunnel thing
  a += sin(2.0*r) - 3.0*cos(2.0+0.1*time);
  vec2 coords = vec2(cos(a),sin(a))/r;

  coords.x += time*speed*.5;

  if(effect == 3.0){   coords.y *= (dur*speed); coords.x *= (dur*speed); }
  if(effect == 4.0){   coords.y *= ((1.-dur)*speed); coords.x *= ((1.-dur)*speed); }

  coords += texOffset;
  vec4 tex = texture(ourTexture, coords) * smoothstep(0.1, 0.2, length(uv));

  // Texture overlay
  if(tex == vec4(0.)){ return inCol; }
  else{ return tex.xyz; }
}


//---
// Texture Sprial
// Produces cool effect on tertures like they're
// waving in the wind, in a spiral of sorts
// Speed: Directly Controls the frequency
// Effect: 1. High Frequency 2. Low Freq 3. High freq to low frequency 4. Low to High
//---
vec3 textureSpiral(vec3 inCol, vec2 uv, vec2 texOffset, float dur, float speed, float effect)
{
  // Make a spiral
  float angle = acos(dot(normalize(uv), vec2(0, -1)))/(3.14);
  if(uv.x > 0.){ angle = angle*.5 + .5; }
  else { angle = (1. - angle)/2.; }

  // Effects
  float repeats = smoothrange(1., .05, speed);//sin(time)*.5 + .5;
  if(effect == 1.0){ repeats = 0.05;}
  if(effect == 2.0){ repeats = 1.;}
  if(effect == 3.0){ repeats = smoothrange(0.05, 1., dur);}
  if(effect == 4.0){ repeats = smoothrange(1., .05, dur);}

  float r = smoothrange(0.00001, 0.9, repeats);
  float length = mod(mod(length(uv), r)+angle+time*.1, r);

  vec2 coords = vec2(TexCoord.x - length, TexCoord.y + length);
  coords += texOffset;
  vec4 tex = texture(ourTexture, coords); // * smoothstep(0.1, 0.2, length(uv));

  // Texture overlay
  if(tex == vec4(0.)){ return inCol; }
  else{ return tex.xyz; }

}



float wave(float x, float j, float speed)
{
  x += j * snoise(vec3 (x*j, time*20.*j, time));
  x += j *sin(time) * snoise(vec3 (time*50., time*50., time*50.));
  x += time  * speed;
  return sin(x) * cos(2*x);
}

//---
// Waveform
// Adjustable waveform to reflect music
// Colour: Colour of the wave
// Pitch: Frequency of the wave
// Volume: Amplitude of the wave
// Jitter: How much the wave moves randomly
// Speed: Horizontal wave speed
// Y: The height of the wave (-.2 to .2)
// Effect: 1. High Frequency 2. Low Freq 3. High freq to low frequency 4. Low to High
//---
vec3 waveform(vec3 inCol, vec2 uv, float colour, float pitch, float volume, float jitter, float speed, float y)
{
  // Smooth range everything to sensible values
  speed = smoothrange(0., 100., speed);
  volume = smoothrange(0., .1, volume);
  pitch = smoothrange(10., 200., pitch);

  float w = wave(uv.x*pitch, jitter, speed)*volume;

  // Makes the wave flat at the edges
  float m = smoothstep(-.5, 0, uv.x) * smoothstep(.5, 0., uv.x);
  w *= m;

  w += y;
  if(uv.y > w && uv.y < w + 0.01 )
  {
    inCol = doCol(colour + uv.x + time);
  }
  return inCol;
}

//---
// Strobe Light
// Stobes entire screen or section of screen
// Should happen for short bursts in time with music
// MsPerBeat: Type in the MsPerBeat from the top section
// Reps: How many strobe lights there are per beat. Numbers smaller than 1 work. Higher than 4 produces constant white
//---
vec3 strobe(vec3 inCol, vec2 uv, float msPerBeat, float reps)
{
  msPerBeat = msPerBeat/1000.;
  msPerBeat /= reps;
  if(mod(time, msPerBeat) > msPerBeat-.05)
  {
    inCol = vec3(1.);
  }
  return inCol;
}


//---
// Fade to black
// Makes the whole screen start out normal and gradually go black
// Duration: How long the fade takes
//---
vec3 fadeToBlack(vec3 inCol, float duration)
{
  return inCol * smoothrange(1., 0., duration*duration);
}



//---
// Visualise Melody
// Horizontal bars play like a piano to a melody
// Colour: The main colour of the bars
// Bars: You will have 1/bars bars. So make it a nice multiple of 1. i.e. 0.05 for 20
// Pitch: What is controlling the pitch
// Intensity: How strong the colour is
// Effect: 1. Squares 2. Moving stripes 3. Moving stripes 2
//---
vec3 melody(vec3 inCol, vec2 uv, float colour, float bars, float pitch, float intensity, float effect)
{
  pitch = float(int(floor(pitch/bars)))*bars; // Round the pitch to the nearest bar

  float p = uv.y + 0.5; //+ 0.5;
  float b = 1.;

  if(effect == 1.0){ b = mod(uv.x, bars); }
  if(effect == 2.0){ b = mod(length((5.*uv.x + time)/(pitch)), 1.0);}
  if(effect == 3.0){ b = sin((uv.y + uv.x - pitch)*100.);}


  if(p > pitch && p < pitch + bars)
  {
      inCol = doCol(colour) * intensity * b;
  }

  return inCol;
}
