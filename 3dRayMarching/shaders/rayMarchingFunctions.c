// Corresponding distance function for the scene
float distSphereScene(vec3 p, int argIndex)
{
  float intensity = effect3D[4 + argIndex];
  float effect = effect3D[5 + argIndex];
  float dispIntensity = effect3D[6 + argIndex];

  vec3 disp = vec3(p.x * 1.2, p.y * 2.3, p.z * 4.6);
  disp *= smoothrange(1.0, 30., dispIntensity);

  if(effect == 1.0){ p.x = mod(p.x+.5+time, 4.0)-4.0/2.;} // repeating in X axis
  if(effect == 2.0){ p.z = mod(p.z+.5+time, 8.0)-8.0/2.; p.x += sin(time/4.)*4.;}
  if(effect == 3.0){ p.z = 0.;} // Inside the sphere


  vec4 sphere = vec4(0.0, 0.0, 0.0, 1.0);
  float d1 = dSphere(p - sphere.xyz, sphere.w);
  float d2 = intensity*0.5*sin(disp.x)*sin(disp.y)*sin(disp.z);
  return d1 + d2;
}


// Corresponding distance function for the scene
float distraveSpheres(vec3 p, int argIndex)
{
  float reps = 50.;
  float effect = effect3D[3 + argIndex];

  if(effect == 1.0){reps = 1.;}
  if(effect == 2.0){reps = 5.;}
  if(effect == 3.0){reps = 10.;}
  if(effect == 4.0){reps = 15.;}
  if(effect == 5.0){reps = 25.;}

  vec4 sphere = vec4(0., 0.0, 0.0, time*100. + 10.);

  return dSphere(p - sphere.xyz, sphere.w);
}

// Corresponding distance function for the scene
float distChainsScene(vec3 p)
{
  float zSpeed = .5;
  float ySpeed = .0;
  float xRepeat = 1.0;
  float zRepeat = .1;


  float xID = floor((p.x-.5*xRepeat)/xRepeat);
  p.x = mod(p.x + .5*xRepeat, xRepeat) - xRepeat*.5;
  p.z += time*abs(xID);

  float id = floor(p.z)-.5;

  p.z = mod(p.z + .5*zRepeat, zRepeat) - zRepeat*.5;

  // Spin the chains
  p.xz = rot(p.xz, ((time+2.9)*PI)/zRepeat);

  // Make the two linked sections
  vec3 a = p; a.y = fract(a.y) - .5;
  vec3 b = p; b.y = fract(b.y+0.5) - .5;

  float c1 = sdLink(a.xyz, 0.1, 0.2, 0.03);
  float c2 = sdLink(b.zyx, 0.1, 0.2, 0.03);

  // Get the min of the two chain segments
  return min(c1, c2);
}

float opSmoothUnion( float d1, float d2, float k)
{
  float h = clamp(0.5 + 0.5 * (d2 - d1) / k, 0.1, 1.0);
  return mix(d2, d1, h) - k * h * (1.0 - h);
}

// Corresponding distance function for the scene
float distPulsingCubesScene(vec3 p)
{
  float xRepeat = 5.0;
  float zRepeat = 5.0;;
  p.x += time;

  p.z += time;

  p.x = mod(p.x + .5*xRepeat, xRepeat) - xRepeat*.5;
  p.z = mod(p.z + .5*zRepeat, zRepeat) - zRepeat*.5;

  float intensity = effect3D[4];
  // Make the two linked sections
  vec3 box = vec3(1.0, 1.0, 1.0);
  vec3 boxPos =  vec3(0.0, 0.0, 0.0);

  vec3 spherePos =  vec3(boxPos.x, boxPos.y + 0.8 + intensity, boxPos.z);

  float roundness = .2;
  float b1 = sdRoundBox(p - boxPos, box, roundness);
  float c1 = dSphere(p - spherePos, 0.9);
  return opSmoothUnion(b1, c1, .3); // + d1*.0001;
}



float distWaveOfCubesScene(vec3 p, int argIndex)
{
  float effect = effect3D[8 + argIndex];
  float r = 1.; // repeats
  float height = effect3D[4 + argIndex]; // How tall they are
  float heightVariance = effect3D[7 + argIndex]; // How much effect noise has on effect
  float speed = effect3D[5 + argIndex]; // Noise speed
  float ns = effect3D[6 + argIndex]; // noise sampling
  float num = 8; // Number of cubes

  speed = smoothrange(0., 10., speed);
  ns = smoothrange(0.05, 0.5, ns);

  if(effect == 1.0){ p = xRot(p, 0.7); p.z -= 20.; p.y += 15.; p = yRot(p, time); }
  if(effect == 2.0){ p = xRot(p, (sin(time*speed)*.5+.5)*3.1) ; p.y += 2.; p.z += 1; }

  float xID = floor((p.x-.5*r) / r);
  float zID = floor((p.z-.5*r) / r);
  float h = (snoise(vec3(xID*ns, xID*ns, zID*ns + speed * time))*.5 + .5) * heightVariance + height;

  float c = 1.0;
  p.x = p.x - c * clamp(round(p.x / c), -num, num);
  p.z = p.z - c * clamp(round(p.z / c), -num, num);

  return sdRoundBox(vec3(p.x, p.y-h, p.z), vec3(.2, h, .2), .1);;
}


float distthreeDFractal1(vec3 p, int argIndex)
{

  float speed = effect3D[argIndex + 4];
  float seed = effect3D[argIndex + 5];
  float effect = effect3D[argIndex + 6];
  float reps = effect3D[argIndex + 7];
  float size = effect3D[argIndex + 8];
  float t = time * speed + seed;
  float r = 1.0;
  float q = 0.0;

  // Rotate properly
  if(effect == 1.0){ p = zRot(p, time); }
  if(effect == 2.0){ p = zRot(p, time); p = xRot(p ,time); }

  // Need to keep track of this for distance function maths
  float scale = 1.0;
  for(int i = 0; i < 25.; i ++)
  {
    vec3 off = vec3(-1., 0., 0.);
    p += off;
    vec3 normal = normalize(vec3(1., 0., 0.));
    p = planeFold(p - off, normal, 0.0);
    p += off;

    // Scale and rotate
    p = zRot(p, t);
    if(effect == 3.0){ p = yRot(p, t); }

    p *= 1.2;
    scale *= 1.2;
  }
  // Draw a single Tetrahedron
  return sdTetrahedron(vec4(p * (1.-size), r), q) / scale;
}



float distthreeDFractal2(vec3 p, int argIndex)
{
  float s = effect3D[argIndex + 5];
  float size = effect3D[argIndex + 6];
  float it = effect3D[argIndex + 7];
  float xR = effect3D[argIndex + 8];
  float yR = effect3D[argIndex + 9];
  float zR = time;

  for(int i = 0; i < int(it); i++)
  {

    p = zRot(p, zR*i);
    p = yRot(p, yR*i);
    p = xRot(p, xR*i);
    p.x -= s;
    p = planeFold(p, vec3(1., 0., 0.), -s);
  }
  return sdRoundBox(p, vec3(size), .1);
}




///////////////////////////////////////
// Each scene has a Corresponding distance function
// This function does all the mapping for this
//////////////////////////////////////
float resolveDistanceFunction(vec3 p, float funcNum, int argIndex)
{
  if(funcNum == 1.0){return distSphereScene(p, argIndex);}
  if(funcNum == 2.0){return distraveSpheres(p, argIndex);}
  if(funcNum == 3.0){return distChainsScene(p);}
  if(funcNum == 4.0){return distPulsingCubesScene(p);}
  if(funcNum == 5.0){return distWaveOfCubesScene(p, argIndex);}
  if(funcNum == 6.0){return distthreeDFractal1(p, argIndex);}
  if(funcNum == 7.0){return distthreeDFractal2(p, argIndex);}
  return 0.0;
}




///////////////////////////////////////
// Get the normal vector of a surface
// This is done by sampling 3 nearby points
// and making a vector out of those
//////////////////////////////////////
vec3 getNormal(vec3 p, float funcNum, int argIndex)
{
  float distance = resolveDistanceFunction(p, funcNum, argIndex);

  vec2 offset = vec2(0.01, 0);

  // We find the normal by taking the distance at 3 surrounding points
  // This gives us a normal of the surface we are looking at
  vec3 normal = distance - vec3(resolveDistanceFunction(p - offset.xyy, funcNum, argIndex),
                                resolveDistanceFunction(p - offset.yxy, funcNum, argIndex),
                                resolveDistanceFunction(p - offset.yyx, funcNum, argIndex));
  // Normals must have a length of 1 or else the lighting will be broken
  return normalize(normal);
}

///////////////////////////////////////
// The lighting is calculated based
// on diffuse lighting like in the Phong Model
//////////////////////////////////////
float doLighting(vec3 p, float funcNum, vec3 light, int argIndex)
{
  vec3 lightDirection = normalize(light - p);
  vec3 normal = getNormal(p, funcNum, argIndex);

  return dot(normal, lightDirection);
}

///////////////////////////////////////
// Core of the whole 3D stuff
// The distance function depends on the scene,
// so we need to have the DI
//////////////////////////////////////
float rayMarch(vec3 start, vec3 direction, float distFunc, int argIndex)
{
  float closeToSurface = 0.01;

  float totalDist = 0.0;
  float thisDist = 0.0;
  vec3 thisPos = start;

  for(int i = 0; i < rayMarchingSteps; i++)
  {
    thisPos = start + direction * totalDist;
    thisDist = resolveDistanceFunction(thisPos, distFunc, argIndex); // Since each scene needs a different distance function
    totalDist += thisDist;
    if(totalDist > MAX_DIST || thisDist < closeToSurface)
    {
      break;
    }
  }
  return totalDist;
}
