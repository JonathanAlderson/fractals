"""
Describes how to draw primitive shapes
and their respective signed distance functions
"""

import pygame
import math
from usefulFunctions import *


class Primitive:


    color = 0   # rendering
    surface = 0 # rendering


    # This is for animating objects
    moveSetup = False
    looping = True
    framesTotal = 0
    framesLeft = 0
    stepX = 0
    stepY = 0

    # these determine how often the primitive repeats
    mirrXRep = 800
    mirrYRep = 300
    mirrX = 150  # this is the position of the mirror relative to the shape

    # this is for the repetitions animation
    mirrXStep = -1
    mirrXStepsTotal = 1000
    mirrXStepsLeft = mirrXStepsTotal

    mirrYStep = 3
    mirrYStepsTotal = 30
    mirrYStepsLeft = mirrYStepsTotal

    def __init__(self):
        """ Each primitive should override this method """
        pass

    def dist(self):
        """ Each primitive should override this method """
        pass

    def draw(self):
        """ Each primitive should override this method """
        pass

    def set_movement(self, xPix, yPix, steps, looping = True):
        """ will move xPix and yPix for n steps
            if looping is true will go back and forth
            will probably be the same for a few classes        """

        self.stepX = xPix
        self.stepY = yPix
        self.looping = looping
        self.framesTotal = steps
        self.framesLeft = self.framesTotal

        self.moveSetup = True

    def update(self):
        """ Does anything the primative needs to do every frames
            will eventually involve doing the colouring and
            any cool rendering effects                           """
        self.move()
        self.draw()

    def move(self):
        """ Each object needs their own move function """
        pass

    def mirrorAnim(self):
        """ Every frame does the animation assosiated with this shape """

        # only if these things have been assigned
        if(self.moveSetup):
            if(self.mirrXStepsLeft > 0):
                self.mirrXStepsLeft -= 1
                self.mirrXRep += self.mirrXStep
            else:
                self.mirrXStep = self.mirrXStep * -1
                self.mirrXStepsLeft = self.mirrXStepsTotal

            if(self.mirrYStepsLeft > 0):
                self.mirrYStepsLeft -= 1
                self.mirrYRep += self.mirrYStep
            else:
                self.mirrYStep = self.mirrYStep * -1
                self.mirrYStepsLeft = self.mirrYStepsTotal



class Line(Primitive):

    # line specific variables
    x1 = 0
    y1 = 0
    x2 = 0
    y2 = 0

    # these determine how often the primitive repeats
    mirrXRep = 300
    mirrYRep = 300
    mirrX = 150  # this is the position of the mirror relative to the shape

    def __init__(self, surface, x1, y1, x2, y2, color = (255, 255, 0), repeats=None):
        if(repeats != None):
            self.mirrXRep = repeats[0]
            self.mirrYRep = repeats[1]
        self.surface = surface
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.color = color

    def move(self):
        """ Moves the position back and forth linearly """
        # only if these things have been assigned
        if(self.moveSetup):
            if(self.framesLeft > 0):
                self.framesLeft -= 1
                self.x1 += self.stepX
                self.x2 += self.stepX
                self.y1 += self.stepY
                self.y2 += self.stepY
            else:
                if(self.looping):
                    # reset and go again
                    self.framesLeft = self.framesTotal
                    self.stepX = self.stepX * -1  #invert
                    self.stepY = self.stepY * -1  #invert
    def dist(self, qx, qy):
        """ Singed distance function to this line """

        if(self.mirrXRep == 0 or self.mirrYRep == 0):
            return 0

        # These beefy lines sort out all the repetitions
        midX = self.x1 + (self.x2 - self.x1) / 2
        midY = self.y1 + (self.y2 - self.y1) / 2
        qx = (qx - midX + self.mirrXRep / 2) % self.mirrXRep - self.mirrXRep / 2 + midX
        qy = (qy - midY + self.mirrYRep / 2) % self.mirrYRep - self.mirrYRep / 2 + midY


        u = [qx - self.x1, qy - self.y1]
        v = [self.x2 - self.x1, self.y2 - self.y1]


        # carry out projections and work out
        # proprions along both the x and y axis
        proportion = clamp(dot_value(u ,v) / dot_value(v, v))
        p = [self.x1 + proportion*(self.x2 - self.x1),
             self.y1 + proportion*(self.y2 - self.y1)]
        q = [qx, qy]

        dx = (q[0] - p[0])
        dy = (q[1] - p[1])

        d = math.sqrt((dy*dy) + (dx*dx))    # Pythag

        return d

    def draw(self):
        """ Draws the shape, but this will not be used much since
            we will be doing ray marching """
        pygame.draw.line(self.surface, self.color, (int(self.x1), int(self.y1)), (int(self.x2), int(self.y2)), 4)



class Circle(Primitive):

    # circle specific variables
    x = 0
    y = 0
    radius = 0


    def __init__(self, surface, x, y, radius, color = (255, 255, 0), repeats=None):
        if(repeats != None):
            self.mirrXRep = repeats[0]
            self.mirrYRep = repeats[1]
        self.surface = surface
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color

    def move(self):
        """ Moves the position back and forth linearly """
        # only if these things have been assigned
        if(self.moveSetup):
            if(self.framesLeft > 0):
                self.framesLeft -= 1
                self.x += self.stepX
                self.y += self.stepY
            else:
                if(self.looping):
                    # reset and go again
                    self.framesLeft = self.framesTotal
                    self.stepX = self.stepX * -1  #invert
                    self.stepY = self.stepY * -1  #invert

    def dist(self, x1, y1):
        """ Singed distance function to this circle """

        if(self.mirrXRep == 0 or self.mirrYRep == 0):
            return 0

        dy = ((self.y - y1)+self.mirrYRep/2)%self.mirrYRep - self.mirrYRep/2
        dx = ((self.x - x1)+self.mirrXRep/2)%self.mirrXRep - self.mirrXRep/2
        d = math.sqrt((dy*dy) + (dx*dx))    # Pythag

        d = d - self.radius             # Then take away radius

        return d

    def draw(self):
        """ Draws the shape, but this will not be used much since
            we will be doing ray marching """
        pygame.draw.circle(self.surface, self.color, (int(self.x), int(self.y)), self.radius, 1)
