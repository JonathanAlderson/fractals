"""
This script sets up our scene, canvas size, ect.
And sets up the user input and game loop.
"""
import sys
from primatives import Circle, Line
from camera import Camera
import pygame
from pygame.locals import *

pygame.init()


width = 1850  #int(input("Screen Width: "))
height = 1000 #int(input("Screen Height: "))

sceneNumber = int(input("Controls\n----------------------\nWASD - Move Camera\nMouse - Aim Camera\nR - Change from no rays, to singe ray, to add rays\nRight Click - Toggle Lock Camera RotationScene Selection\n----------------------\n\n1.Circle\n2. Circle repeating in the X axis\n3. Circle repeating in both axes\n4. Lines scene\n5. Animation Scene\n6. Complicated Scene\n\n\n:"))

fps = 60
fpsClock = pygame.time.Clock()


screen = pygame.display.set_mode((width, height))


#################
# Scene Setup
#################
# Object surface, x, y, radius, colour

objects = []

if(sceneNumber == 1):
    x = int(width / 2)
    y = int(height / 2)
    size = int(min(width, height) * .1)
    repeats = size * 100
    objects.append(Circle(screen, x, y, size, (0, 150, 0), (repeats, repeats)))

elif(sceneNumber == 2):
    x = int(width / 2)
    y = int(height / 2)
    size = int(min(width, height) * .1)
    repeats = size * 3
    objects.append(Circle(screen, x, y, size, (0, 150, 0), (repeats, 10000)))

elif(sceneNumber == 3):
    x = int(width / 2)
    y = int(height / 2)
    size = int(min(width, height) * .1)
    repeats = size * 3
    objects.append(Circle(screen, x, y, size, (0, 150, 0), (repeats, repeats)))

elif(sceneNumber == 4):
    # Makes a pentagon
    repeats = (800, 800)
    objects.append(Line(screen, 0, -300, -285, -93, (148, 0, 211), repeats))
    objects.append(Line(screen, -285, -93, -176, 243, (0, 0, 255), repeats))
    objects.append(Line(screen, -176, 243, 176, 243, (0, 255, 0), repeats))
    objects.append(Line(screen, 176, 243, 285, -93, (255, 255, 0), repeats))
    objects.append(Line(screen, 285, -93, 0, -300, (255, 0, 0), repeats))


elif(sceneNumber == 5):
    repeats = (800, 800)
    objects.append(Line(screen, 0, -300, -285, -93, (148, 0, 211), repeats))
    objects.append(Line(screen, -285, -93, -176, 243, (0, 0, 255), repeats))
    objects.append(Line(screen, -176, 243, 176, 243, (0, 255, 0), repeats))
    objects.append(Line(screen, 176, 243, 285, -93, (255, 255, 0), repeats))
    objects.append(Line(screen, 285, -93, 0, -300, (255, 0, 0), repeats))
    for i in range(len(objects)):
        objects[i].set_movement(5, 0, 1000, True)

elif(sceneNumber == 6):
    repeats = (900, 900)
    repeats2 = (1000, 1000)
    objects.append(Line(screen, 0, -300, -285, -93, (148, 0, 211), repeats))
    objects.append(Line(screen, -285, -93, -176, 243, (0, 0, 255), repeats))
    objects.append(Line(screen, -176, 243, 176, 243, (0, 255, 0), repeats))
    objects.append(Line(screen, 176, 243, 285, -93, (255, 255, 0), repeats))
    objects.append(Line(screen, 285, -93, 0, -300, (255, 0, 0), repeats))
    objects.append(Circle(screen, 0, 0, 100, (255, 0, 0), repeats2))
    objects.append(Circle(screen, 0, 0, 100, (255, 0, 0), repeats2))
    objects[-1].set_movement(5, 0, 1000, True)
    objects[-2].set_movement(0, 5, 1000, True)





sceneCamera = Camera(screen, 150, 150)


# Mouse
mousePos = pygame.mouse.get_pos()
keyboard = {'w': False,
            'a': False,
            's': False,
            'd': False,
            'r': False}


def manageInputs():
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    mousePos = pygame.mouse.get_pos()

    rightClick = pygame.mouse.get_pressed()[2]
    keys = pygame.key.get_pressed()
    keys = [keys[119],keys[97],keys[115],keys[100],keys[114], rightClick]
    keys = [bool(key) for key in keys] # convert 0 -> false 1 -> true

    keyboard = {'w': keys[0],
                'a': keys[1],
                's': keys[2],
                'd': keys[3],
                'r': keys[4],
                'rightClick': keys[5]}
    return mousePos,keyboard

# Game loop.
while True:
    screen.fill((255, 255, 255))

    mousePos,keyboard = manageInputs()
    # Update.
    for obj in objects:
        obj.update()

    sceneCamera.update(mousePos, keyboard, objects)
    sceneCamera.draw()

    # Draw.

    pygame.display.flip()
    fpsClock.tick(fps)
