///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
// DISTANCE FUNCTIONS
//////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////




///////////////////////////////////////
// DISTANCE TO A LINE SEGMENT
//////////////////////////////////////
float dLineSeg(vec2 p, float lineMin, float lineMax)
{
  p.x -= max(lineMin, min(lineMax, p.x));  // Clamp the x dist to the min max
  return length(p);                        // Y height does not need to change
}


///////////////////////////////////////
// DISTANCE TO A SPHERE
//////////////////////////////////////
float dSphere(vec3 p, float radius)
{
  return (length(p) - radius);
}




///////////////////////////////////////
// ROTATIONS AND FOLD
//////////////////////////////////////
vec3 planeFold(vec3 z, vec3 n, float d)
{
  return z - 2.0 * min(0.0, dot(z.xyz, n) - d) * n;
}

vec3 xRot(vec3 p, float angle)
{
  mat3 rot;
  rot[0] = vec3(1., 0.,          0.);
  rot[1] = vec3(0., cos(angle), -sin(angle));
  rot[2] = vec3(0., sin(angle),  cos(angle));
  return inverse(rot) * p;
}

vec3 yRot(vec3 p, float angle)
{
  mat3 rot;
  rot[0] = vec3(cos(angle),  0., sin(angle));
  rot[1] = vec3(0.,          1., 0.        );
  rot[2] = vec3(-sin(angle), 0., cos(angle));
  return inverse(rot) * p;
}

vec3 zRot(vec3 p, float angle)
{
  mat3 rot;
  rot[0] = vec3(cos(angle), -sin(angle), 0.);
  rot[1] = vec3(sin(angle),  cos(angle), 0.);
  rot[2] = vec3(0.,          0.,         1.);
  return inverse(rot) * p;
}
