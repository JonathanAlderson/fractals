#pragma once
#include <vector>
#include <string>

std::string readFile(std::string fileName);
std::string makeShaders();
std::vector<float> getAnimData(std::string song, std::string version);
std::vector<float> getColourData(std::string song, std::string version);
std::tuple<std::string, std::string, std::string, std::string> readSettings(std::string fileName);
std::tuple<float, float, float, float, std::string, float, float> readRenderSettings(std::string fileName);
std::vector<std::string> getImages(std::string fileName);
