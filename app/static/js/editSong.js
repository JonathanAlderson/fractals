// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function hexToRgb(hex)
{
  // This is a hex to rgb converter. I took this off some stack overflow
  // answer
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

function getNum(name)
{
  // Just some regex stuff
  return parseInt(document.getElementsByClassName(name)[0].textContent.match(/\d+/g));
}


function addEffect(songName, songVersion)
{
  // Adds an effect and saves the song
  saveFile(songName, songVersion, -1)
}

function removeEffect(songName, songVersion, number)
{
  // Removes and effect from the list and saves the song
  saveFile(songName, songVersion, number - 1)
}


function getArguments(arguments)
{
  // Just splits by newline
  // In future versions this could check if the number
  // of arguments was correct but it doesn't really matter
  // since if there's too many they never go into the program
  // and if there's not enogh they're just 0's so everyone is happy
  // we also need to ignore spaces
  var args = arguments.value.split("\n")
  var validArgs = []
  for(i = 0; i < args.length; i++)
  {
    if(args[i] != ""){ validArgs.push(args[i])}
  }
  console.log(args)
  console.log(validArgs)
  return(validArgs)
}

function getEffectID(effect)
{
  // Formats the ID so our program is happy
  var id = (parseInt(effect.value) - 1).toString()
  // Invalid value, so we can't save the song
  if(id == -1)
  {
    return -1
  }
  if(id.length == 2){ id = "0" + id }
  if(id.length == 1){ id = "00" + id }
  return id
}

Number.prototype.roundTo = function(num) {
    var resto = this%num;
    if (resto <= (num/2)) {
        return this-resto;
    } else {
        return this+num-resto;
    }
}

function roundNearest(number, msPerBeat)
{
  number = number * 1000. // convert from ms to seconds
  number = parseInt(number).roundTo(parseInt(msPerBeat))
  number = number / 1000.0 // convert back
  return (number.toString())
}

function syncStartAndEnd(songName, songVersion, number, msPerBeat)
{
  // Round these two numbers to the nearest beat so all the
  // fx are in sync and look grooving, you know what I mean
  // then save it
  var id = number - 1
  var startTime = roundNearest(document.getElementsByClassName("timeStartSelection")[id].value, msPerBeat);
  var endTime = roundNearest(document.getElementsByClassName("timeEndSelection")[id].value, msPerBeat);

  document.getElementsByClassName("timeStartSelection")[id].value = startTime
  document.getElementsByClassName("timeEndSelection")[id].value = endTime
  //saveFile(songName, songVersion)
}

function saveFile(songName, songVersion, effectIndex = -2)
{
  // -2 means don't change effects
  // -1 means add a new effect to the end
  //  n means remove that effect

  var validSong = 1 // Only save if it's a valid song
  var errorMessage = "" // Make a big error message as to why things fail

  var colours = document.getElementsByClassName("colourSelection");
  var effects = document.getElementsByClassName("effectSelection");
  var startTimes = document.getElementsByClassName("timeStartSelection");
  var endTimes = document.getElementsByClassName("timeEndSelection");
  var arguments = document.getElementsByClassName("argumentsSelection");
  var i;

  // Setup the DICT which we will dump to a json file
  // Here we split the info and get just the numbers out of them
  var structData = {"info"     : {"bpm"        : getNum("bpmSelection"),
                                  "msPerBeat"  : getNum("msPerBeatSelection"),
                                  "songLength" : getNum("songLengthSelection")},
                    "settings" : {"durationFalloff" : parseInt(document.getElementById("durationSelection").value),
                                  "pitchFalloff"    : parseInt(document.getElementById("pitchSelection").value),
                                  "velocityFalloff" : parseInt(document.getElementById("velocitySelection").value)},
                    "colours" : [],
                    "effects" : []};

  // Add all the colours in
  for (i = 0; i < colours.length; i++)
  {
    var col = hexToRgb(colours[i].value);
    structData["colours"].push([col.r, col.g, col.b]);
  }

  // Add all the effects in
  for(i = 0; i < effects.length; i++)
  {
    var thisID = getEffectID(effects[i])
    if(thisID == -1 && i != effectIndex)
    {
      validSong = 0
      errorMessage += " Effect " + (i + 1).toString() + " invalid effect type"
    }

    var effect = {"id"    : thisID,
                  "start" : parseInt((startTimes[i].value)*1000),
                  "end"   : parseInt((endTimes[i].value)*1000),
                  "args"  : getArguments(arguments[i])
                  }
    structData["effects"].push(effect);
  }

  // Remove an effect
  if(effectIndex > -1)
  {
    structData["effects"].splice(effectIndex, 1)
  }

  // Add a new effect
  if(effectIndex == -1)
  {
    emptyEffect = {"id"    : "001",
                   "start" : 0,
                   "end"   : 0,
                   "args"  : "0"
                  }
    structData["effects"].push(emptyEffect);
  }

  // Find out how much playback is delayed by
  var playbackStart = document.getElementById("playbackStart").value;

  // Find out how fast to playback
  var playbackSpeed = document.getElementById("playbackSpeed").value;

  // Send back the data
  // So flask can save the file and update it
  if(validSong == 1)
  {
    $.ajax({
      type: "POST",
      contentType: "application/json;charset=utf-8",
      url: "/editSong/".concat(songName, "/", songVersion, "/", playbackStart, "/", playbackSpeed),
      traditional: "true",
      data: JSON.stringify(structData),
      dataType: "json"
    }).done(window.location.href = ("/editSong/" + songName + "/" + songVersion + "/" + playbackStart + "/" + playbackSpeed));
    document.getElementById("automationErrorMessage").innerHTML = ""
  }
  else
  {
    document.getElementById("automationErrorMessage").innerHTML = errorMessage
  }

}
