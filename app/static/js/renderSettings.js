function editRenderSettings()
{
  // -2 means don't change effects
  // -1 means add a new effect to the end
  //  n means remove that effect

  var fullscreen = document.getElementById("fullscreenSelect").checked;
  var wait = document.getElementById("waitSelect").value;
  var sync = document.getElementById("syncSelect").value;
  var mute = document.getElementById("muteSelect").checked;
  var framerate = document.getElementById("framerateSelect").value;
  var record = document.getElementById("recordSelect").checked;
  var steps = document.getElementById("stepsSelect").value;


  if(fullscreen == true){ fullscreen = "1.0"}
  else{ fullscreen = "0.0"}

  if(mute == true){ mute = "1.0"}
  else{ mute = "0.0"}

  if(record == true){ record = "1.0"}
  else{ record = "0.0"}

  var settings = [fullscreen, wait, sync, mute, framerate, record, steps]

  $.ajax({
    type: "POST",
    contentType: "application/json;charset=utf-8",
    url: "/renderSettingsSave",
    traditional: "true",
    data: JSON.stringify(settings),
    dataType: "json"
  }).done(window.location.href = ("/renderSettings"));

}
