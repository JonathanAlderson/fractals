//---
// Rotate Screen
// Rotates the screen n times over a duration
// Arguments are (n - n) and not (0 - 1)
// Rotations: How many rotations happen over the period
// Smoothing: Set to 1.0 for effect. Gradual speed up and slow down.
//---
vec2 screenRotation(vec2 uv, float dur, float r, float smoothing)
{
  if(smoothing == 0.0)
  {
    dur = smoothrange(0., 1., dur);
  }
  uv = rot(uv, 6.28*dur*r);
  return uv;
}

//---
// Zoom In-Out
// Zooms out and back in the entire screen
// Arguments are (n - n) and not (0 - 1)
// Repeats: How many times the effect happends over the duration
// Min: How zoomed in we are at full amount. 1 is normal
// Max: How many times zoomed out at full amount. 4 is standard
// In-Out: 0 to zoom in from Min to Max, 1 to zoom from 1/Min to 1/Max
//---
vec2 screenZoom(vec2 uv, float dur, float r, float min, float max, float inOut)
{
  // You can see how this function works below
  // https://www.desmos.com/calculator/fzaxcc8arc
  //float zoom = min + max*sin(((3.14*time)/dur)*r);//min + (sin((time)/dur)*max);
  float zoom = min + max*sin(r*(3.14*dur));
  if(inOut == 1)
  {
    zoom = 1./zoom;
  }
  return fract(uv*(zoom) + .5) - .5;
}

//---
// Screen Slice
// Splits the screen up into rows or columns
// and moves them around in a lovely fashion
// First three args (0 - 1) mode (1, 2, 3, 4, 5)
// Speed: How fast the segments move up and down
// SliceSize: The smaller the size, the more slices
// Scale: How far each slice moves
// Mode: 0 - Vertical, 1 - Horizontal, 2 - Scanlines, 3 - Moving Scanlines
//---
vec2 screenSlice(vec2 uv, float speed, float sliceSize, float scale, float mode)
{
  speed = smoothrange(0.1, 10., speed);
  scale = smoothrange(0.05, 5.0, scale);

  float numberOfSlices = smoothrange(2.0, 200.0,sliceSize);

  float idX = floor(uv.x*numberOfSlices*2.);
  float idY = floor(uv.y*numberOfSlices*4.);

  if(mode == 0.0){ uv.y += sin(time*speed + idX*.5) * scale; }
  if(mode == 1.0){ uv.x += sin(time*speed + idY*.5) * scale; }

  if(mode == 2.0)
  {
    idX = mod(floor(uv.x*200.), 2.0);
    if(idX == 1.0)
    {
      uv.y += sin(time*speed + idX*.5) * scale;
    }
  }

  if(mode == 3.0)
  {
    idX = mod(floor(uv.x*200.), 4.0);
    idY = mod(floor(uv.x*200.), 4.0);
    if(idX == 1.0)
    {
      uv.y += sin(time*speed + idX*.5) * scale;
    }
    if(idY == 1.0)
    {
      uv.x += cos(time*speed + idY*.5) * scale;
    }
  }
  return uv;
}


//---
// Texture Distort
// Effects all following texture effects
// appliest distortion effects to texture
// Speed: Speed the texture moves
// Amount: How much effect the distortion has
// Effect: 1. Normal Waving 2. Horizontal Waving 3. Vertical Waving 4. Chaotic Distortion 5. Matrix Lines 6. Grid 7. Rotate eg Enter 7 + your spin speed, so 50% spin is 7.50
// Effect2: 1. Slow to Fast 2. Fast to Slow (For other effects this means zooming out or zooming in)
//---
vec2 textureDistort(vec2 uv, float dur, float speed, float amount, float effect, float effect2)
{

  amount = smoothrange(0., 5., amount);

  if(effect2 == 1.0){ amount = dur * amount; }
  if(effect2 == 2.0){ amount = (1. - dur) * amount; }

  float noise = snoise(vec3 (uv.xy, time * speed));


  if(effect == 2.0){ noise = snoise(vec3 (uv.xx, time * speed)); } // x Only
  if(effect == 3.0){ noise = snoise(vec3 (uv.yy, time * speed)); } // y Only
  if(effect == 4.0){ noise = snoise(vec3 (uv.x * abs(sin(time)) * 20. * amount, uv.y, time * speed)); } // Wierd
  if(effect == 5.0){ noise = snoise(vec3 (floor(uv.x * 125.52 * amount), uv.y, time * speed)); } // Matrix
  if(effect == 6.0){ noise = snoise(vec3 (floor(uv.x * amount * 100.), floor(uv.y * amount * 100.), time * speed));} // Grid
  if(effect > 7.0 && effect < 8.0){ return(rot(uv - .5*uv, time*30.*(sin(time*.1)*.5+.1) * (effect-7.0))); }


  noise *= amount;

  return vec2(noise);

}
