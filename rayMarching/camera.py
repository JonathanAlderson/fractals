"""
This class controls the camera operation.
The camera is drawn by a square and a triangle connected to eachother
as a rough approximation. The size of the camera lens does not
reflect the field of view of the camera as they are seperated,
and this camera is just so the user  can get a rough idea of Where
the rays are coming from.
This handles keyboard input for moving the camera.
And the code for ray marching.
"""


import pygame
import time
from usefulFunctions import *
class Camera:

    # Where the camera is
    x = 0
    y = 0
    rotation = math.radians(180) # The cameras initial rotation

    # Where the camera is looking
    tx = 0
    ty = 0

    # cameras coordinates
    width = 25
    height = 15
    speed = 5
    coords = [(-width, height), (0, height), (0 ,0), (width, height),
              (width, -height), (0, 0), (0, -height), (-width, -height)]
    rays = 10
    maxIterations = 10
    fov = 0.6

    # brush
    color = (0, 0, 0)
    lineWidth = 2

    # vars
    allowRotate = True
    allowMove = True
    cooldown = 0
    drawCircles = False



    def __init__(self, surface, x, y, rays = 50, fov = 1.0, drawCircles = False):

        self.surface = surface
        self.x = 0
        self.y = 0
        self.updatePositon(x, y) # initialise the position
        self.rays = rays
        self.fov = fov
        self.drawCircles =  drawCircles

    def updatePositon(self, dX, dY):
        """ Moves the camera horizontally or vertically"""
        self.x += dX
        self.y += dY
        for i in range(len(self.coords)):
            self.coords[i] = (self.coords[i][0] + dX, self.coords[i][1] + dY)

    def move(self, keyboard):
        """ Takes the keyboard input to move the camera around the scene """
        x = 0
        y = 0
        if(self.allowMove):
            if(keyboard['w']):
                y -= self.speed
            if(keyboard['s']):
                y += self.speed
            if(keyboard['a']):
                x -= self.speed
            if(keyboard['d']):
                x += self.speed

            self.updatePositon(x,y)

        if(keyboard['r'] and self.cooldown == 0):
            if(self.drawCircles == True):
                self.drawCircles =  False

            elif(self.drawCircles == "Maybe"):
                self.drawCircles = True

            elif(self.drawCircles == False):
                self.drawCircles = "Maybe"
            self.cooldown = 30


    def followMouse(self, mouseX, mouseY, keyboard):
        """ Lets the camera follow the mouse """

        # Stop rotating on right click
        if(keyboard['rightClick'] and self.cooldown == 0):
            self.allowRotate = not self.allowRotate # cheeky
            self.cooldown = 60
            print("Stopped rotating")
        if(self.cooldown > 0):
            self.cooldown -= 1

        if(self.allowRotate):
            angle = self.calcAngle(mouseX, mouseY)
            self.coords = rotate_polygon(self.coords, self.x, self.y, -1*angle) # rotate camera
            self.rotation -= angle # update the value of the cameras rotaton

    def calcAngle(self, x, y):
        """ gets angle between the mouse and camera """
        dy = y - self.y
        dx = x - self.x
        if(dy == 0 or dx == 0):
            return 0
        angle = math.atan(dy/dx)
        diff = (self.rotation - angle)
        # Adjust so if knows weather mouse is left or right
        # of the camera
        if(dx > 0):
            diff -= math.radians(180)
        return diff

    def getdistances(self, objects, x = None, y = None):
        """ Finds shortest distance to any object in the scene """
        if(x == None):
            x = self.x
        if(y == None):
            y = self.y
        shortestDist = 9999
        closestObject = None
        for o in objects:
            thisDist = o.dist(x, y)
            if(thisDist < shortestDist):
                shortestDist = thisDist
                closestObject = o
        #print(shortestDist)
        return [shortestDist,closestObject]

    def rayMarch(self, mousePos, keyboard, objects, offset):
        """ Incrementalls marches a ray to see what object it collides with """
        maxSize = 400
        tolerence = 0.2
        objPos = [[self.x, self.y]]
        sceneObjs = []
        objRadii = []

        rotation = self.rotation + offset

        # if the user presses r
        for i in range(self.maxIterations):
            x1 = objPos[-1][0]
            y1 = objPos[-1][1]

            objDistInfo = self.getdistances(objects, x1, y1)
            minDist = objDistInfo[0]
            if(minDist > maxSize):
                break
            sceneObjs.append(objDistInfo[1])
            objRadii.append(minDist) # how big this circle should be

            x2 = x1 - math.cos(rotation)*minDist
            y2 = y1 - math.sin(rotation)*minDist
            objPos.append([x2, y2]) # where the next one should start


        for i in range(len(objRadii)):
            color = (0, 0, 0, 255)
            x = int(objPos[i][0])
            y = int(objPos[i][1])
            radius = int(objRadii[i])
            width = 1
            if(radius <= tolerence):

                pygame.draw.circle(self.surface, sceneObjs[i].color, (x, y), 5)
                break
            if(self.drawCircles == True or (self.drawCircles == "Maybe" and offset == 0)):
                pygame.draw.circle(self.surface, color,(x, y), radius, width)

    def draw(self):
        """ Draws the shape, but this will not be used much since
            we will be doing ray marching """
        pygame.draw.polygon(self.surface, self.color, self.coords, self.lineWidth)

    def update(self, mousePos, keyboard, objects):
        """ Will rotate the camera to face where the mouse is """

        self.followMouse(mousePos[0], mousePos[1], keyboard)
        self.move(keyboard)
        #print("start")
        for i in range(self.rays):
            a = -(self.fov/2) + i*(self.fov/(self.rays-1))
            if a > 0 and a < (self.fov/(self.rays-1)):
                a = 0
            self.rayMarch(mousePos, keyboard, objects,a)
