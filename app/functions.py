import os, json, shutil, sys, time

def getAllSongs():
    """Returns a string of all the songs we can edit"""
    return(next(os.walk("data"))[1])

def getAllSongVersions(songTitle):
    """ Given a song, finds all the versions of the song"""
    allSongVersions = next(os.walk("data/" + songTitle))[1]
    validSongVersions = []
    for v in allSongVersions:
        if(v != "images"): # Images is a reserved word since it stores the images
            validSongVersions.append(v)
    return(validSongVersions)

def addNewSongVersion(songTitle, newSongVersion):
    """ Given a song and a new version,
        will create a new json from the template in the file. And redirect
        the user to pick a version again                                   """
    # Make a new directory for this version of the song
    destination = "data/" + songTitle + "/" + newSongVersion
    os.mkdir(destination)
    # Copy the existing JSON data
    source = "data/" + songTitle + "/structureData.json"
    shutil.copy2(source, destination)
    return(next(os.walk("data/" + songTitle))[1])

def getAllInstruments(songTitle):
    """ Given a song, finds the names of all the objects in
        the scene """
    dir = next(os.walk("data/" + songTitle))
    files = dir[2]
    instruments = []
    for f in files:
        if(f.endswith(".mid") and not f.startswith(songTitle)):
            f = f.split(".")[0]
            instruments.append(f)
    return instruments

def getAllImages(songTitle):
    """ Find all the images that can be used for this song """
    with open("data/" + songTitle + "/images/allImages.txt", "r") as f:
        images = f.read()
    images = images.split("\n")
    images = [i.split(".")[0] for i in images]
    return images


def getRenderSettings():
    """ Reads the settings from the file """
    settings = []
    with open("3dRayMarching/renderSettings.txt", "r") as f:
        settings = f.read().split("\n")[:-1]
    settings = [float(i) for i in settings]
    return settings

def saveRenderSettings(settings):
    """ Saves render settings into file """
    with open("3dRayMarching/renderSettings.txt", "w") as f:
        for s in settings:
            f.write(s)
            f.write("\n")


def getAutomationData(songTitle, songVersion):
    """ Given a song title and an object name, will get all the automation
        data in a dictionary format, we are reading them in from .txt files """
    # Reads the text file of the dictionary and turns it into an actual dictionary
    # where we have each variable, and the points that make up the automation
    # for it
    with open("data/" + songTitle + "/" + songVersion + "/structureData.json") as f:
        try:

            data = json.load(f)
        except:
            time.sleep(0.2)
            data = json.load(f)
            print(f.read())
            print("--- ERROR ---")

    # Change all the arguments to be one string of newlines
    for effect in data["effects"]:
        effect["args"] = ("\n").join(effect["args"])

    # Also open up all the function data
    effectData = getAllEffects()

    return [data, effectData]

def checkSongStatus(songTitle, songVersion):
    """ See if there are any broken effects with the song """
    status = execute(4, [songTitle, songVersion])
    if(status.startswith("Generated anim data")): # no error to report
        return ("")
    else:
        return(status)

def saveAutomationData(songTitle, songVersion, data, playbackStart, playbackSpeed):
    """ Given some new data and a place to save it. We update the old data """

    # Error/Completion Messages
    toReturn = ""
    # Write new json to a file
    try:
        with open("data/" + songTitle + "/" + songVersion + "/structureData.json", "w+") as f:
            json.dump(data, f, indent = 2)
            toReturn += "JSON Saved\n"
    except:
        toReturn += "JSON Failed\n"

    # Write the song title and verison to the CPP code
    try:
        with open("3dRayMarching/settings.txt", "w") as f:
            f.write(songTitle)
            f.write("\n")
            f.write(songVersion)
            f.write("\n")
            f.write(playbackStart)
            f.write("\n")
            f.write(playbackSpeed)
            toReturn += "Settings Saved\n"
    except:
        toReturn += "CPP Song Name Failed\n"

    # Update the anim data
    toReturn += execute(4, [songTitle, songVersion]) + "\n"

    # Since we've just written to the file.
    # We can read to it to refresh the page
    return getAutomationData(songTitle, songVersion) + [toReturn]

def getCustomAutomations(newName = None, newPoints = None):
    """ Reads through the file of stored created automations
        if we give it a name we add it at the same time
        if we give a name and data, we update the data      """

    print("Get Custom Automations: ", newName, " ", newPoints)
    # Why can't all languagues work this nicely with JSON.
    # This takes me 2 minutes in Python and 4 hours in CPP
    with open("data/customAutomations.json", "r") as f:
        data = json.load(f)

    customAutomations = data["automations"]
    names = [i["name"] for i in customAutomations]
    points = [i["points"] for i in customAutomations]


    if(newName != None):
        # Update current info
        if(newPoints != None):
            for customAutomation in data["automations"]:
                if(customAutomation["name"] == newName):
                    # Turns our points from [1,2,3,4] to [[1, 2], [3, 4]]
                    # like our format requires#
                    newPoints = list(eval(newPoints))
                    newPoints = [round(i, 3) for i in newPoints]
                    customAutomation["points"] = [newPoints[i:i+2] for i in range(0, len(newPoints), 2)]
        # Add new info
        else:
            pointData = [[0, 0], [1, 1]] # Default starting point
            data["automations"].append({"name": newName, "points": pointData})
            names.append(newName)
            points.append(pointData)
            print(data)
            print(names)
            print(points)
        # Write the new data back
        with open("data/customAutomations.json", "w") as f:
            json.dump(data, f, indent = 2)

    return [names, points]

def getAllEffects():
    """ Gets all the effects """
    # Open the function data
    with open("DataGenerator/effectInfo.json") as f:
        effectData = json.load(f)

    effectData["activeTexture"] = [{'title': "Set Active Texture",
                                    'description': ['Changes which texture is to be used for the texture functions.',
                                                    'Only the start value matters.',
                                                    'When the time passes the start time, the texture name in the first argument will be the active texture.',
                                                    'Texture must be selected from images list.',
                                                    'Images must be PNG with an alpha layer and of aspect ratio 16:9.'],
                                    'args': ['Name of Texture: Filename of the texture you want to be active']}]

    return effectData


def runProgram():
    """ Tells the CPP to do it's thing """
    # Somehow this just works and Python just
    # doens't care. You can even open up multiple
    # instances.
    run = os.system("cd 3dRayMarching && ./MusicVisualiser && cd ..")
    error = ""
    if(run != 0):
        error = "Unknown Error"
    if(run == 34304):
        error = "Invalid Start or End time"
    return(error)



def updateCurrentSong(songTitle, songVersion):
    """ The CPP program always reads from the text file
        so when we select a song version and play a video
        it always plays the right one                     """
    with open("3dRayMarching/settings.txt", "w") as f:
        f.write(songTitle)
        f.write("\n")
        f.write(songVersion)
        f.write("\n")
        f.write("0.0")
        f.write("\n")
        f.write("1.0")

def execute(action, optinalArgs=[]):
    """ Calls one of the Python scripts previously made """
    # This is a nice bit of the codebase.
    # It just imports everything else as a library and does
    # it's thing and reports back to the website
    print("Action: ", action)
    action = int(action)

    # Get all the paths added so we can import the programs
    path = os.getcwd() + "/DataGenerator"
    sys.path.insert(1, path)
    path = os.getcwd() + "/MIDI"
    sys.path.insert(1, path)
    path = os.getcwd() + "/fft"
    sys.path.insert(1, path)

    # Update the effect info
    if(action == 1):
        import effectInfoGenerator
        # has to be ran twice because of MIDI formats
        effectInfoGenerator.updateEffectInfo()
        return(effectInfoGenerator.updateEffectInfo())

    # Update the MIDI info
    if(action == 2):
        import generateMetadata
        return(generateMetadata.main())

    # Update the FFT info
    if(action == 3):
        import fftData
        return(fftData.main())

    # Update the animation data
    if(action == 4):
        import animDataGenerator
        return(animDataGenerator.main(optinalArgs))

    # Update the image data
    if(action == 5):
        import imageDataGenerator
        return(imageDataGenerator.main())

    # Does them all just to be sure
    if(action == 6):
        import effectInfoGenerator, generateMetadata, animDataGenerator, fftData, animDataGenerator, imageDataGenerator
        toReturn = ""
        effectInfoGenerator.updateEffectInfo()
        toReturn += effectInfoGenerator.updateEffectInfo()
        toReturn += "\n\n\n"
        toReturn += generateMetadata.main()
        toReturn += "\n\n\n"
        toReturn += fftData.main()
        toReturn += "\n\n\n"
        toReturn += animDataGenerator.main()
        toReturn += "\n\n\n"
        toReturn += imageDataGenerator.main()
        return toReturn

    return "Ran Successfully"
