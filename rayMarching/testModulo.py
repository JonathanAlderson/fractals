"""
This file looks into modular arithmetic,
just used to get to grips with how repetitions
work in a modular space
"""
import random


mirrYRep = 300
posx = 0

def a(x):
    return ((x - posx)+mirrXRep/2)%mirrXRep - mirrXRep/2

def b(x):
    #return ((x - posx)+mirrXRep/2)%mirrXRep - mirrXRep/2
    mirrorX = 50
    reflectPoint = mirrXRep - mirrorX # 250

    mirrX2 = mirrXRep - mirrorX

    if(x < mirrorX * 2):
        # do modulus function for the first reflection
        return ((x - posx)+mirrorX)%(mirrorX*2) - mirrorX
    else:
        return (x- posx)%(mirrXRep - 2 * mirrorX) - 2 * mirrorX


def c(x):
    x1 = 0
    mirrXRep = 300
    mirrX = 50
    if((x - x1)%(mirrXRep) < (2 * mirrX)):

        #dx = 50
        dx = ((x - x1)+mirrX)%(2 * mirrX) - mirrX
    else:

        x = x % mirrXRep

        dx = ((x - x1)%(mirrXRep - (2 * mirrX))) - 2 * mirrX

    return dx


def d(y, y1):
    dy = ((y - y1)+mirrYRep/2)%mirrYRep - mirrYRep/2
    return dy

def e(y, y1):
    y = (y+mirrYRep/2) % mirrYRep - mirrYRep/2
    dy = (y - y1)
    return dy


def f(x):
    mirrXRep = 300
    x = x % mirrXRep
    return x

def h(x):
    centre = 20
    mirrXRep = 300
    x = (x - centre + mirrXRep / 2) % mirrXRep - mirrXRep / 2

    return x


g = [-150, -100, -50, 0, 50, 100, 150, 170]

print([h(x) for x in g])
