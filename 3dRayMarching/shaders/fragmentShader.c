void main()
{
    // Since we can have lots of functions going on at once
    // This keeps track of who was calling who
    int offset = 0;
    // Normalized Pixel Coordinate
    vec2 uv = vec2((gl_FragCoord.x-.5*screen.x)/screen.y, (gl_FragCoord.y-.5*screen.y)/screen.y);
    uv *= .5;

    // Output colour for this pixel
    vec3 col = vec3(0.);   // Final Colour

    // Distorted by any texture distortion functions
    // and used by every texture rendering function
    vec2 texOffset = vec2(0.);

    ////////////////////////////////////
    // Screen Effects
    ////////////////////////////////////
    for(offset = 0; offset <= 20; offset += 10)
    {
      // Rotations (Must be done before anything else)
      if(effectScreen[0 + offset] == 0.0){ uv = screenRotation(uv, effectScreen[2 + offset], effectScreen[3 + offset], effectScreen[4 + offset]);}
    }
    for(offset = 0; offset <= 20; offset += 10)
    {
      // Zoom Out (Must be done next)
      if(effectScreen[0 + offset] == 1.0){ uv = screenZoom(uv, effectScreen[2 + offset], effectScreen[3 + offset], effectScreen[4 + offset], effectScreen[5 + offset], effectScreen[6 + offset]);}
    }
    for(offset = 0; offset <= 20; offset += 10)
    {
      // Screen Slice
      if(effectScreen[0 + offset] == 2.0){ uv = screenSlice(uv, effectScreen[3 + offset], effectScreen[4 + offset], effectScreen[5 + offset], effectScreen[6 + offset]);}
      // Texture Distort
      if(effectScreen[0 + offset] == 3.0){ texOffset = textureDistort(uv, effectScreen[2 + offset], effectScreen[3 + offset], effectScreen[4 + offset], effectScreen[5 + offset], effectScreen[6 + offset]);}
    }

    ////////////////////////////////////
    // Background Effects
    ////////////////////////////////////
    for(offset = 0; offset <= 20; offset += 10)
    {
      // Background Colour
      if(effectBackG[0 + offset] == 100.0){ col = col + effectBackG[1 + offset] * solidColour(effectBackG[3 + offset], effectBackG[4 + offset]);}
      // Gradient Colour
      if(effectBackG[0 + offset] == 101.0){ col = col + effectBackG[1 + offset] * gradientColours(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset], effectBackG[6 + offset]);}
      // Shifting Colours
      if(effectBackG[0 + offset] == 102.0){ col = col + effectBackG[1 + offset] * shiftingColours(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset], effectBackG[6 + offset], effectBackG[7 + offset]);}
      // seperateColours
      if(effectBackG[0 + offset] == 103.0){ col = col + effectBackG[1 + offset] * seperateColours(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset], effectBackG[6 + offset], effectBackG[7 + offset], effectBackG[8 + offset]);}
      // tunnel
      if(effectBackG[0 + offset] == 104.0){ col = col + effectBackG[1 + offset] * tunnel(uv, effectBackG[3 + offset], effectBackG[4 + offset]);}
      // chaotic fractal
      if(effectBackG[0 + offset] == 105.0){ col = col + effectBackG[1 + offset] * chaoticFractal(uv, effectBackG[3 + offset], effectBackG[4 + offset]);}
      // inky
      if(effectBackG[0 + offset] == 106.0){ col = col + effectBackG[1 + offset] * inky(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset]);}
      // Ripple
      if(effectBackG[0 + offset] == 107.0){ col = col + effectBackG[1 + offset] * ripple(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset], effectBackG[6 + offset], effectBackG[7 + offset]);}
      // mandlebrotSet
      if(effectBackG[0 + offset] == 108.0){ col = col + effectBackG[1 + offset] * mandlebrotSet(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset], effectBackG[6 + offset], effectBackG[7 + offset], effectBackG[8 + offset], effectBackG[9 + offset]);}
      // juliaSet
      if(effectBackG[0 + offset] == 109.0){ col = col + effectBackG[1 + offset] * juliaSet(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset], effectBackG[6 + offset], effectBackG[7 + offset], effectBackG[8 + offset], effectBackG[9 + offset]);}
      // drum
      if(effectBackG[0 + offset] == 110.0){ col += drum(uv, effectBackG[3 + offset], effectBackG[4 + offset], effectBackG[5 + offset], effectBackG[6 + offset], effectBackG[7 + offset], effectBackG[8 + offset], effectBackG[9 + offset]); }
    }

    ////////////////////////////////////
    // 3D Effects
    ////////////////////////////////////
    for(offset = 0; offset <= 20; offset += 10)
    {
      // Sphere
      if(effect3D[0 + offset] == 200.0){ col = col + effect3D[1 + offset] * sphereScene(uv, effect3D[2 + offset], effect3D[3 + offset], offset);}
      // Sphere Scene 2
      if(effect3D[0 + offset] == 201.0){ col = col + effect3D[1 + offset] * raveSpheres(uv, effect3D[2 + offset], effect3D[3 + offset], effect3D[4 + offset], effect3D[4 + offset], offset);}
      // Chains Scene
      if(effect3D[0 + offset] == 202.0){ col = col + effect3D[1 + offset] * chains(uv, effect3D[3 + offset], effect3D[4 + offset], effect3D[5 + offset], offset);}
      // Pulsing Cubes
      if(effect3D[0 + offset] == 203.0){ col = col + effect3D[1 + offset] * pulsingCubes(uv, effect3D[3 + offset], effect3D[4 + offset], offset);}
      // Wave of Cubes
      if(effect3D[0 + offset] == 204.0){ col = col + effect3D[1 + offset] * waveofCubes(uv, effect3D[3 + offset], effect3D[8 + offset], offset);}
      // fractal
      if(effect3D[0 + offset] == 205.0){ col = col + effect3D[1 + offset] * threeDFractal1(uv, effect3D[3 + offset], offset); }
      // 2nd fractal
      if(effect3D[0 + offset] == 206.0){ col = col + effect3D[1 + offset] * threeDFractal2(uv, effect3D[3 + offset], effect3D[4 + offset], offset); }

    }

    ////////////////////////////////////
    // Filter Effects
    ////////////////////////////////////
    for(offset = 0; offset <= 20; offset += 10)
    {
      // Fade from black
      if(effectFilter[0 + offset] == 300.0){col = fadeFromBlack(col, effectFilter[2 + offset]);}

      // Static Texture
      if(effectFilter[0 + offset] == 301.0){col = staticTexture(col, texOffset, effectFilter[3 + offset], effectFilter[4 + offset], effectFilter[5 + offset], effectFilter[6 + offset], effectFilter[7 + offset]);}
      // Texture Warp Tunnel
      if(effectFilter[0 + offset] == 302.0){col = warpTunnel(col, uv, texOffset, effectFilter[2 + offset], effectFilter[3 + offset], effectFilter[4 + offset]);}
      // Texture Spiral
      if(effectFilter[0 + offset] == 303.0){col = textureSpiral(col, uv, texOffset, effectFilter[2 + offset], effectFilter[3 + offset], effectFilter[4 + offset]);}
      // Waveform
      if(effectFilter[0 + offset] == 304.0){col = waveform(col, uv, effectFilter[3 + offset], effectFilter[4 + offset], effectFilter[5 + offset], effectFilter[6 + offset], effectFilter[7 + offset], effectFilter[8 + offset]);}
      // Strobe
      if(effectFilter[0 + offset] == 305.0){col = strobe(col, uv, effectFilter[3 + offset], effectFilter[4 + offset]);}
      // Fade to black
      if(effectFilter[0 + offset] == 306.0){col = fadeToBlack(col, effectFilter[2 + offset]);}
      // melody
      if(effectFilter[0 + offset] == 307.0){ col = melody(col, uv, effectFilter[3 + offset], effectFilter[4 + offset], effectFilter[5 + offset], effectFilter[6 + offset], effectFilter[7 + offset]); }

    }

    FragColor =  vec4(col.xyz, 1.0);
}
