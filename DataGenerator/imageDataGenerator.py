"""
This file is created because implementing
something simple in CPP is harder than it should be.
This file browses a directory and stores an alphabetically
sorted list of image names for each song.
We must keep this list up to date since when we update
a texutre on the CPP end. We must map it to the correct one.
"""
import os


def getAllSongs(data):
    """ Gets every song in data """
    songs = [x[1] for x in os.walk(data)][0]
    return      songs


def getAllImages(data, song):
    """ Get all the songs from the data store """
    images = [x[2] for x in os.walk(data + "/" + song + "/images")]
    if(len(images) > 0):
        images = images[0]
        validImages = []
        for i in images:
            if(i.endswith(".png")):
                validImages.append(i)
        return validImages
    else:
        return []

def saveFile(data, song, images):
    """ Writes down the alphabetically sorted list of images
        into a text file. For the website and CPP program to use. """
    images = sorted(images)
    with open(data + "/" + song + "/images/allImages.txt", "w+") as f:
        for i in range(len(images)):
            f.write(images[i])
            f.write("\n")

def main():
    data = "../data"
    toReturn = ""
    if(os.getcwd().endswith("fractals")):
        data = data[3:]
    songs = getAllSongs(data)
    for song in songs:
        toReturn += song
        images = getAllImages(data, song)
        saveFile(data, song, images)

        if(len(images) == 0):
            toReturn += " no images"
        else:
            toReturn += " " + str(len(images)) + " images"
        toReturn += "\n"
    return toReturn

if(__name__ == "__main__"):
    main()
