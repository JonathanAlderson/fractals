
//---
// BackGround Colour
// This is the most simple effect
// Having a solid colour that can match
// a melody or rhythm. A great starting point.
// Colour: Which colour
// Strength: How bright the colour is
//---
vec3 solidColour(float colour, float strength)
{
  strength = strength*.8 + .2; // Put in a range we like
  vec3 col = doCol(colour);
  col *= strength;
  return col;
}

//---
// Gradient Colours
// Have nice moving gradients between a colour and it's complementary
// Colour: The colour of the background
// Strength: The intensity of the complementaries
// Speed: How fast the horizontal movement is
// Scale: How zoomed out from the blobs you are
//---
vec3 gradientColours(vec2 uv, float colour, float strength, float speed, float scale)
{
  speed = smoothrange(0., 5., speed);
  uv *= smoothrange(1., 5., scale);

  vec3 col = doCol(colour);
  vec3 col2 = doColComp(colour);

  float mixAmount = strength * smoothstep(0., 1., snoise(vec3(uv.x + time*speed, uv.y, time*.3)));
  col = mix(col, col2, mixAmount); // * strength;

  return col;
}


//---
// Shifting Colours
// Blobs continuously move over the screen as the background
// Tie strength to a melody or rhythm.
// Scale, speed and blur also work well for pitch or velocity.
// Colour: The colour of the background
// Strength: The intensity of the colour, going from dark to bright
// Speed: How fast all the blobs move.
// Scale: The size of the dots
// Blur: The sharpness or blurriness of the dots
//---
vec3 shiftingColours(vec2 uv, float colour, float strength, float speed, float scale, float blur)
{

  blur = smoothrange(0., 3., blur);
  scale = smoothrange(.5, 100., scale);
  speed = smoothrange(.1, 5., speed);
  strength = smoothrange(.2, 1.0, strength); // Put in a range we like

  vec3 col = doCol(colour) * strength;
  col += smoothstep(0., blur, snoise(scale*vec3(uv, (time*speed)/scale)));

  return col;
}

//---
// Seperate Colours
// Show 3 seperate colour flashings in each third of the screen.
// Like a Neapolitan ice cream
// Colour 1: Left hand side colour
// Colour 2: Middle colour
// Colour 3: Right hand side colour
// Strength 1: Left hand side strength
// Strength 2: Middle strength
// Strength 3: Right hand side strength
//---
vec3 seperateColours(vec2 uv, float colour1, float colour2, float colour3, float strength1, float strength2, float strength3)
{
  //vec3 strengthCol = vec3(strength, strength2, strength3);
  vec3 col1 = doCol(colour1) * smoothrange(.2, 1., strength1);
  vec3 col2 = doCol(colour2) * smoothrange(.2, 1., strength2);
  vec3 col3 = doCol(colour3) * smoothrange(.2, 1., strength3);


  if(uv.x <= .5 && uv.x > .1666) { return col1;}
  if(uv.x <= .1666 && uv.x > -.1666){ return col2;}
  if(uv.x <= -.1666 && uv.x >= -.5)  { return col3;}

  return col1;
}

//---
// Tunnel
// 3D Effect Tunnel
// Repeats: How many tunnel rings pass each beat
// Effect: Choose between preset effects, 1. Sideways Pan, 2. Vertical Pan, 3. Both, 4. Top Right, 5. Bottom Left
//---
vec3 tunnel(vec2 uv, float r, float effect)
{
  if(effect == 1.0){uv.x -= sin(time)*.2;}
  if(effect == 2.0){uv.y -= cos(time)*.2;}
  if(effect == 3.0){uv.y -= cos(time)*.2; uv.x -= sin(time)*.4;}
  if(effect == 4.0){uv -= vec2(.4, .25);}
  if(effect == 5.0){uv -= vec2(-.4, -.25);}
  float dist = length(uv);
  dist = fract(1./dist + fract(time)*4.*r);
  dist -= smoothstep(1.0, 0.0, length(uv)*5.);

  return vec3(dist, dist, dist);
}

//---
// Chaotic Fractal
// Speed: How chaotic the effect is
// Opacity: How visible the effect is
//---
vec3 chaoticFractal(vec2 uv, float chaoticness, float opacity)
{
  float radius = atan(uv.x, uv.y);
  float dist = 1. - length(uv);
  float choticness = .1;
  float num =  time*choticness + abs(uv.x) + abs(uv.y);
  float curLine = floor(num * 100.*chaoticness)*dist*.001;
  float speed = sin(curLine * 24.3);
  float c = step(.4, tan(radius + speed * time ));
  return vec3(c)*opacity;
}


//---
// Ink Effect
// Puts a moving layer of ink over the screen
// Colour: The main colour of the ink
// Intensity: The amount of ripples on the sreen
// Effect: The effect type; 1: Pan out from center 2: Moving 3: Colour Changing, 4: Intensity controls colour
//---
vec3 inky(vec2 uv, float colour, float intensity, float effect)
{
  float scale = 40.;
  float ySpeed = 10.;
  int amount = 3;// = int(floor(time*2.));
  float xSpeed = 2.;
  float ySpeed2 = 12.0;

  if(effect == 1.0){ xSpeed = .0; ySpeed = 0; scale = time*10.;}
  if(effect == 2.0){ scale = 10; ySpeed = 2; amount = 10; }
  if(effect == 3.0){ colour+= time;}
  if(effect == 4.0){ colour += intensity; intensity = sin(time*.1)*.2 + .5;}

  float len;
  uv *= scale;
  uv.y += time*ySpeed;

  for(int i = 0; i < amount; i ++)
  {
    len = length(uv);
    uv.x = uv.x - sin(uv.y + sin(len)) + time*xSpeed;
    uv.y = uv.y + cos(uv.x+ cos(len)) + sin(time / ySpeed2);
  }

  //return vec3(intensity);
  return doCol(colour) * vec3(cos(pow(len, 0.5 + intensity)));
}


//---
// Ripple
// Makes a Rippling action on the screen with a
// kaelidoscopic fractal as the centre
// Colour: Main Colour
// WaveFrequency: How often a wave ripples out from the center
// XOffset: The origin of the effect in the X Axis
// YOffset: The origin of the effect in the Y Axis
// Effect: 1. Rotate 2. Zoom 3. Rotate and Zoom
//---
vec3 ripple(vec2 uv, float colour, float waveFrequency, float xOffset, float yOffset, float effect)
{
  float speed = .5;
  waveFrequency = smoothrange(0.1, 2.0, waveFrequency);//sin(time)*.5 + .5;
  speed = smoothrange(0.1, 10.0, speed);

  int iterations = 4;

  if(effect == 1.0 || effect == 3.0) {uv = rot(uv, time); }// Rotation
  if(effect == 2.0 || effect == 3.0) { uv *= 0.1*mySin(1., 10., 0., .48); }     // Zoom
  if(effect == 3.0){ uv += vec2(sin(time), cos(time)); }// Pan



  uv *= 30.; // Now numbers go from -1 to 1
  uv.x = abs(uv.x);
  vec2 firstTransform = vec2(1.5, 1.0);
  uv -= firstTransform;
  uv = fold(uv, (4./3.) * PI);
  uv.x += firstTransform.x;
  float foldAmount = mySin(-1.06, PI/6., 0., 1.);
  uv = kaelidoscopeFold(uv, iterations, foldAmount);

  // Logic for effect
  uv -= vec2(xOffset, yOffset);

  // Draw line from 0. to 1.
  uv = vec2(dLineSeg(uv, 0., 1.));

  // Colouring

  vec3 col = mod(vec3(length(uv * waveFrequency)) * 0.1 + speed * time, 1.0) * doCol(colour);
  col -= length(uv)*.001; // darkens edges

  return col;
}


//---
// Mandlebrot Set
// A nice interactive rendition of the mandlebrot set
// Colour: The starting colour of the bulb
// Speed: How fast the camera moves
// Intensity: Controls the shape of the fractal
// Effect: 1. Zoom in and out 2. Colour cycle 3. Camera move
// X: X Position
// Y: Y Position
// Offset: Changes the cameras start position
//---
vec3 mandlebrotSet(vec2 uv, float colour, float speed, float intensity, float effect, float x, float y, float offset)
{
  float col = 0.0;
  intensity = smoothrange(0.04, 0.1, intensity);//

  int iterations = int(intensity*300.);

  vec2 pos = vec2(x, y);

  if(effect == 1.0){ uv *= sin(time*.1 + offset)*.5 + .5; }
  if(effect == 2.0){ col = time + offset;}
  if(effect == 3.0){ pos += vec2((sin(time*speed + offset)*.5+.5)*1.8 -1.25,  (cos(time*speed + offset)*.5+.5)*1.25 - .6); }

  uv += pos;
  vec2 c = uv;
  vec2 z = uv;

  for(int i = 0; i < iterations; i++)
  {
    z = cmul(z, z) + c;
  }
  float len = length(z)*5.;

  return(doCol(mod((len + col) * colour, 5.0) ));
}



//---
// Julia Set
// A nice interactive rendition of the Julia set
// Colour: The starting colour of the bulb
// Speed: How fast the camera moves
// Intensity: Controls the shape of the fractal
// Effect: 1. Zoom in and out 2. Colour cycle 3. Camera move
// X: X Position
// Y: Y Position
// Offset: Changes the cameras start position
//---
vec3 juliaSet(vec2 uv, float colour, float speed, float intensity, float effect, float x, float y, float offset)
{
  float col = 0.0;
  intensity = smoothrange(0.04, 0.1, intensity);//

  int iterations = int(time);// int(intensity*300.);

  x = 0;
  y = 0;
  vec2 pos = vec2(x, y);

  uv *= 5.;
  uv += pos;
  vec2 c = uv + vec2(time, 0.);
  vec2 z = uv;

  for(int i = 0; i < iterations; i++)
  {
    z = csin(z) + c;
  }
  float len = length(z)*100.;
  return(doCol(mod((len + col) * colour, 5.0) ));
}




//---
// Visualise Drum
// Circle pulses outwards when a hit it registered
// Colour: The main colour of the Circle
// Intensity: Intensity of the colour
// Hit: When it was hit
// Blur: How sharp the edges are
// X: X position
// Y: Y positon
// Scale: How Large it is
//---
vec3 drum(vec2 uv, float colour, float intensity, float hit, float blur, float x, float y, float scale )
{
  uv -= vec2(x, y);
  vec3 col = doCol(colour);
  col *= intensity * smoothstep(scale, (scale - 0.001)*(1. - blur), length(uv)/hit);

  return col;
}
