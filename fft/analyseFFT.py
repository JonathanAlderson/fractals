"""
This file shows the first test at tracking the bass guitar
and seeing how well it can detect notes from it
"""

import time


# Read file
file = open("BassOut.txt", "r").read()
lines = file.split("\n")
for i in range(len(lines)):
    lines[i] = lines[i].split(",")
lines = lines[:-1]

# turn everything from strings to floats
for i in range(len(lines)):
    lines[i] = [float(lines[i][0]), float(lines[i][1])]


# normalize the data (Only 2nd element)
minV = min([i[1] for i in lines])
maxV = max([i[1] for i in lines])
for i in range(len(lines)):
    lines[i][1] = (lines[i][1]-minV) / (maxV - minV)

# Find the average change in volume between samples#
averageDiff = 0
for i in range(1, len(lines)):
    averageDiff += abs(lines[i][1] - lines[i-1][1])
averageDiff = averageDiff / len(lines)

print("Average Difference: ", averageDiff)

# For analysing
currentNote = 0
foundLouder = False # Found a louder note in the next 5
keepLooking = False # Even if there is not a big gap, look at this set

# Threshold values
diffThresh = 1.0    # This is to see if the change in volume is a lot
lookAhead = 5       # How many samples in the future we look to see if there is louder
volumeThresh = 0.93 # Note volume has to be above this threshold

# Playback
start = 0
interval = 1/60
timer = 0





for i in range(int(start * (1/interval)) + 1,len(lines)):
    # Timing
    time.sleep(max(interval - timer, 0))
    timerStart = time.time()

    # Check if this sample is louder than the previous
    # and if the increase is noteworthy or not
    # and if is loud enough in the first place
    thisDiff = lines[i][1] - lines[i - 1][1]
    if(lines[i][1] > volumeThresh and ((thisDiff > averageDiff * diffThresh) or keepLooking == True)):

        # Check to see if 'soon' there will be a peak much higher
        # soon is about 5 samples
        foundLouder = False
        for j in range(lookAhead):
            if(lines[i + 1 + j][1] > lines[i][1]):
                foundLouder = True


        if(foundLouder):
            print(round(i * interval, 2), "\t", i, "\t", "Diff Threshold", "\t", lines[i][1])
            #print(round(i * interval, 2))
            keepLooking = True
            continue

        # This is the next forseeable loudest note
        else:
            currentNote = lines[i][0]
            print(round(i * interval, 2), "\t", lines[i][1], "\t", currentNote, "<-----------" )

            keepLooking = False

    # Just print out time
    else:
        print(round(i * interval, 2), "\t", lines[i][1], "\t", lines[i][0])

    #Timing
    timer = time.time() - timerStart
