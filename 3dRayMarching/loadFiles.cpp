#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <iterator>
#include <vector>
#include <sstream>
#include <tuple>
#include <stdio.h>
#include <ctype.h>
#include "loadFiles.h"

// Reads a file in through a buffered reader
std::string readFile(std::string fileName)
{
  std::ifstream in(fileName.c_str());
  std::string file((std::istreambuf_iterator<char>(in)),
                    std::istreambuf_iterator<char>());
  return file;
}

// Reads settings line by line and gives them back as a tuple
std::tuple<std::string, std::string, std::string, std::string> readSettings(std::string fileName)
{
  std::string song;
  std::string version;
  std::string startTime;
  std::string playbackSpeed;
  std::ifstream in(fileName.c_str());
  std::getline(in, song);
  std::getline(in, version);
  std::getline(in, startTime);
  std::getline(in, playbackSpeed);
  std::cout << "Song: " << song << std::endl;
  std::cout << "Version: " << version << std::endl;
  std::cout << "Start: " << startTime << std::endl;
  std::cout << "Speed: " << playbackSpeed << std::endl;

  return std::make_tuple(song, version, startTime, playbackSpeed);
}

// Reads render settings line by line and gives them back as a tuple
std::tuple<float, float, float, float, std::string, float, float> readRenderSettings(std::string fileName)
{
  std::string fullscreen;
  std::string wait;
  std::string sync;
  std::string mute;
  std::string framerate;
  std::string record;
  std::string rayMarchingSteps;
  std::ifstream in(fileName.c_str());
  std::getline(in, fullscreen);
  std::getline(in, wait);
  std::getline(in, sync);
  std::getline(in, mute);
  std::getline(in, framerate);
  std::getline(in, record);
  std::getline(in, rayMarchingSteps);

  std::cout << "Fullscreen: " << fullscreen << std::endl;
  std::cout << "Wait: " << wait << std::endl;
  std::cout << "Sync: " << sync << std::endl;
  std::cout << "Mute: " << mute << std::endl;
  std::cout << "Recording Framerate: " << framerate << std::endl;
  std::cout << "Recording: " << record << std::endl;
  std::cout << "Ray Marching Steps: " << rayMarchingSteps << std::endl;



  return std::make_tuple(std::stof(fullscreen), std::stof(wait), std::stof(sync), 1. - std::stof(mute),
                         framerate, std::stof(record), std::stof(rayMarchingSteps));
}

// Reads from the allImages.txt file to get file names of this songs images
std::vector<std::string> getImages(std::string fileName)
{
  std::ifstream in(fileName.c_str());
  std::string line;
  std::vector<std::string> images;
  while(std::getline(in, line))
  {
    images.push_back(fileName.substr(0, fileName.length() - 14) + "/" + line);
  }
  return images;
}

// Get's animation data as a vector of floats
std::vector<float> getAnimData(std::string song, std::string version)
{
  std::string animDataDir = "../data/" + song + "/" + version + "/animData.txt";
  std::string data = readFile(animDataDir);
  std::vector<float> v;  // The return vector
  std::istringstream iss(data); // String stream of data

  // Iterate over stream and get the floats out of it
  std::copy(std::istream_iterator<float>(iss),
            std::istream_iterator<float>(),
            std::back_inserter(v));
  return v;
}

// Reads from JSON to get colour data
std::vector<float> getColourData(std::string song, std::string version)
{
  std::string structDataDir = "../data/" + song + "/" + version + "/structureData.json";
  std::vector<float> allNumbers; // The return vector
  std::vector<float> allColours;
  std::string data = readFile(structDataDir);

  // I tried and tried to get a JSON library working,
  // but you know how CPP is, so I'm just doing it like
  // a Neanderthal. We're just finding the start and end of the
  // colours by hunting for '[[' and ']]'. And then doing
  // some parsing and converting. All very grim stuff.
  // after some debugging. I can't even get CPP to find [[ and ]]
  // probably because of some newline characters. Best I can do
  // is find all numbers than take 6, 20, because I know those indexes
  // to be the numbers. I wish CPP was a nicer language.


  // Used as a bool to see where an int starts and ends
  int reading = 0;
  int numStart = 0;

  for(unsigned int i = 0; i < data.size(); i++)
  { // Start reading because we've found an int
    if(isdigit(data[i]) && reading == 0)
    {
      reading = 1;
      numStart = i;
    }
    // Stop reading because it's over
    if(!isdigit(data[i]) && reading == 1)
    {
      reading = 0;
      // get sub string, convert to int, add to list
      allNumbers.push_back(std::stoi(data.substr(numStart, i - numStart)));
    }
  }
  // return array of 15 colours
  for(unsigned int i = 0; i < allNumbers.size(); i++)
  {
    if(i > 5 && i < 21)
    {
      allColours.push_back(allNumbers[i]);
    }
  }
  return allColours;
}

// Puts all the shaders together
// in one file from all their sources
// and puts them together

std::string makeShaders()
{

  // All the files we need to put together
  std::vector<std::string> shaders;
  shaders.push_back("shaders/shaderFunctions.c");
  shaders.push_back("shaders/externalFunctions.c");
  shaders.push_back("shaders/distanceFunctions.c");
  shaders.push_back("shaders/screenEffects.c");
  shaders.push_back("shaders/backgroundEffects.c");
  shaders.push_back("shaders/rayMarchingFunctions.c");
  shaders.push_back("shaders/3DEffects.c");
  shaders.push_back("shaders/filterEffects.c");
  shaders.push_back("shaders/fragmentShader.c");

  std::string out;

  for(unsigned int i = 0; i < shaders.size(); i++)
  {
    out = out + readFile(shaders[i]);
  }
  return out;
}
