"""
Just a small script to look at portions of MIDI data
"""

import sys, os, pickle, time
from mido import MidiFile
from mido import tick2second
from mido import Message
from mido import MidiTrack
from enum import Enum

song = sys.argv[1] # "Underwater Techno"

start = int(sys.argv[2])
end = int(sys.argv[3])
print(song)


data = "../data/" + song
metadata = "../data/" + song + "/metadata.txt"
structure = "../data/" + song + "/structureData.json"

line = 0

mid = MidiFile(data)
print(mid.type)
time.sleep(1)
for i, section in enumerate(mid.tracks):
    print("\n\n\n\n")
    for msg in section:
        if(line < end and line > start):
            print(msg)
        line += 1


# This is the point in time where I was getting
# the error where everything was 2.5 times as fast as it should be
# and not sure why. Looking at the diff checker everything seems fine :/

# This error was because some of the metadata of the track isn't stored
# as a message. So when I was copying all the messages from a track and
# saving it. I wouldn't put the bpm message back in. So it would always get
# the speed wrong. This was a very annoying bug to fix
