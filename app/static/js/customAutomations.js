// List of Points
class PointList
{

  constructor(pointsIn, scale = 1.0)
  {
    ctx.font = '11pt Arial';

    this.oldMouse = new Point(0, 0);
    this.border = 0.10;
    this.pointsSize = 8;
    this.points = []
    this.movingPoint = -1; // Index of the point that is being moved by the mouse
    this.zoom = 1.;
    this.scale = scale;
    console.log(this.scale)
    ctx.scale(this.scale, this.scale)
    this.zoomCentre = new Point(canvasSize.x / 2, canvasSize.y / 2);
    this.currentAutomation = "Not Set Yet"


    for (var i = 0; i < pointsIn.length; i++)
    {
      this.points.push(new Point(pointsIn[i][0], pointsIn[i][1]));
    }

    this.minX = 0;
    this.minY = 0;
    this.maxX = 0;
    this.maxY = 0;

    this.maxYHeight = 1;
    this.minYHeight = 0;


    this.update();
  }

  // Replaces all the current points with some new ones
  replaceAllPoints(newPoints, newName)
  {
    newPoints = eval(newPoints)
    this.currentAutomation = newName // must keep track of this for when we save
    this.points = [] // goodbye old points
    for (var i = 0; i < newPoints.length; i++)
    {
      this.points.push(new Point(newPoints[i][0], newPoints[i][1]));
      // hello shiny new points
    }
    this.update()
  }

  // draws everything to the screen
  update()
  {
    ctx.clearRect(0, 0, canvas.width / .5 , canvas.height / .5);
    this.updateMinMax();
    this.drawPoints();
    this.joinPoints();
    this.markings();
  }
  // Find new minX, maxX, minY, maxY
  updateMinMax()
  {
    var flag = 1; // only lets us go when all the points are in order
    while(flag == 1)
    {
      flag = 0;
      for (var i = 1; i < this.points.length; i++)
      {
        if(this.points[i-1].x > this.points[i].x)
        {
          // swap the points around
          flag = 1;
          if(this.movingPoint == i){ this.movingPoint -= 1; }
          else if(this.movingPoint == i-1){ this.movingPoint += 1; }
          [this.points[i-1], this.points[i]] = [this.points[i], this.points[i-1]];
        }
      }

    }
    // We've all seen these 1000 times before
    var minX = 999;
    var minY = 999;
    var maxX = -999;
    var maxY = -999;
    for (var i = 0; i < this.points.length; i++)
    {
      if(this.points[i].x < minX) { minX = this.points[i].x}
      if(this.points[i].x > maxX) { maxX = this.points[i].x}
      if(this.points[i].y < minY) { minY = this.points[i].y}
      if(this.points[i].y > maxY) { maxY = this.points[i].y}
    }
    this.minX = minX - (maxX - minX) * this.border;
    this.maxX = maxX + (maxX - minX) * this.border;
    this.minY = minY - (maxY - minY) * this.border;
    this.maxY = maxY + (maxY - minY) * this.border;
  }

  // draws a circle on each point
  drawPoints()
  {

    var thisPoint;
    var canvasPoint;
    for (var i = 0; i < this.points.length; i++)
    {
      ctx.fillStyle = '#000000';
      thisPoint = this.points[i];
      canvasPoint = this.calculateCanvasPoint(thisPoint);

      if(i == this.movingPoint){ctx.fillStyle = '#0000FF';}
      ctx.beginPath();
      ctx.arc(canvasPoint.x, canvasPoint.y, this.pointsSize, 0, Math.PI * 2);
      ctx.fill();
    }
  }

  // draws a straight line between all of the points
  joinPoints()
  {
    var thisPoint;
    var canvasPoint;
    ctx.beginPath();
    for (var i = 0; i < this.points.length; i++)
    {
      thisPoint = this.points[i];
      canvasPoint = this.calculateCanvasPoint(thisPoint);

      ctx.lineTo(canvasPoint.x, canvasPoint.y);
    }
    ctx.stroke();
  }

  // The scale markings to show the grid
  markings()
  {
    var x;
    var y;
    var lineBottom;
    for (var i = 0; i < 10; i++)
    {
      ctx.strokeStyle = "#D3D3D3";
      x = this.minX + (this.maxX - this.minX) * (i+1)/10;
      y = this.minY + (this.maxY - this.minY) * (i+1)/10;
      lineBottom = this.calculateCanvasPoint(new Point(x, y));

      ctx.beginPath();
      ctx.lineTo(lineBottom.x, 40);
      ctx.lineTo(lineBottom.x, canvasSize.y - 25);
      ctx.stroke();

      ctx.beginPath();
      ctx.lineTo(100, lineBottom.y);
      ctx.lineTo(1100, lineBottom.y);
      ctx.stroke();
      ctx.strokeStyle = '#000000';
      ctx.strokeText(Math.round(x * 100) / 100, lineBottom.x - 10, canvasSize.y - 10);
      ctx.strokeText(Math.round(y * 100) / 100, 70, lineBottom.y);
    }
  }

  // Go from variable space to canvas space
  calculateCanvasPoint(p)
  {
    var x;
    var y;
    var final = new Point(0, 0);
    // Get from the variables we have to the canvas we have
    x = ((p.x - this.minX) / (this.maxX - this.minX)) * canvasSize.x;
    y = canvasSize.y - ((p.y - this.minY) / (this.maxY - this.minY)) * canvasSize.y;
    // Doing the zoom calculations
    final.x = (x - this.zoomCentre.x) * this.zoom + this.zoomCentre.x;
    final.y = (y - this.zoomCentre.y) * this.zoom + this.zoomCentre.y;
    return(final);
  }

  // Go from canvas space to variable space
  calculateVariablePoint(p)
  {
    var x;
    var y;
    var edge = 90;
    // Stops people going too fast
    if(p.x > canvasSize.x - edge)
    {
      p.x = canvasSize.x - edge
    }
    x = (p.x / canvasSize.x) * (this.maxX - this.minX) + this.minX;
    y = (this.maxY - this.minY) - ((p.y / canvasSize.y) * (this.maxY - this.minY)) + this.minY;
    return(new Point(x, y));
  }


  // See if the mouse is clicking on a point
  clickPoint(x, y)
  {
    console.log("Start");
    var rect = canvas.getBoundingClientRect(); // Take into acc pos on web page
    x = x - rect.left;
    y = y - rect.top;
    var mousePos = new Point(x, y); // The pos of mouse on the canvas, not in canvas space
    console.log(mousePos);

    for (var i = 0; i < this.points.length; i++)
    {
      if(i == 1){console.log(this.calculateCanvasPoint(this.points[i]));}

      if(this.mouseDistance(this.calculateCanvasPoint(this.points[i]), mousePos) <= (this.pointsSize * this.pointsSize))
      { this.movingPoint = i; }
    }
  }

  // Removes a point
  deletePoint()
  {
    for (var i = 0; i < this.points.length; i++)
    {
      if(this.mouseDistance(this.calculateCanvasPoint(this.points[i]), mouse) <= (this.pointsSize * this.pointsSize))
      { this.points.splice(i, 1); break;}
    }
    this.update();
  }

  addPoint()
  {
    this.points.push(this.calculateVariablePoint(mouse));
    this.update();
  }

  unclickPoint()
  {
    this.movingPoint = -1;
    this.update();
  }

  movePoint()
  {
    var variableSpacePoint;
    if(this.movingPoint != -1)
    {
      variableSpacePoint = this.calculateVariablePoint(mouse);
      this.points[this.movingPoint].x = variableSpacePoint.x;
      this.points[this.movingPoint].y = variableSpacePoint.y;
      if(this.points[this.movingPoint].x < 0) { this.points[this.movingPoint].x = 0;}
      if(this.points[this.movingPoint].x > songLength) { this.points[this.movingPoint].x = songLength;}
      if(this.points[this.movingPoint].y > this.maxYHeight) { this.points[this.movingPoint].y = this.maxYHeight - 0.001}
      if(this.points[this.movingPoint].y < this.minYHeight) { this.points[this.movingPoint].y = this.minYHeight;}
      this.update();
    }
    this.oldMouse.x = mouse.x;
    this.oldMouse.y = mouse.y;
  }

  // Need to take into account that circles are drawn form their
  // top left
  mouseDistance(a, b)
  {
    a.x += this.pointsSize / 2
    a.y += this.pointsSize / 2
    return ((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
  }

  updateMouse(x, y)
  {

    //x -= this.pointsSize;
    //y -= this.pointsSize;
    var rect = canvas.getBoundingClientRect();
    x = x - rect.left;
    y = y - rect.top;
    x = x / this.scale;
    y = y / this.scale;
    mouse = new Point((x - this.zoomCentre.x) / this.zoom + this.zoomCentre.x ,
                      (y - this.zoomCentre.y) / this.zoom + this.zoomCentre.y);
  }
  zoomIn()
  {
    // Zoom in a bit more and focus on where the mouse is
    this.zoom += 0.1;
    this.zoomCentre = new Point(mouse.x, mouse.y); // needed to pass by refernece
    this.update();
  }
  zoomOut()
  {
    if(this.zoom > 1)
    {
      this.zoom -= 0.1;
      this.zoomCentre = new Point(mouse.x , mouse.y);
      this.update();
    }
  }

  setMaxHeight()
  {
    this.maxYHeight = document.getElementById("maxHeight").value;
    this.update();
  }

  setMinHeight()
  {
    this.minYHeight = document.getElementById("minHeight").value;
    this.update();
  }


  save()
  {
    console.log("Saving")
    console.log(this.currentAutomation)
    console.log(this.points)
    var listOfPoints = []
    for (var i = 0; i < this.points.length; i++)
    {
      listOfPoints.push([this.points[i].x, this.points[i].y]);
      // hello shiny new points
    }
    $.ajax({
      type: "POST",
      contentType: "application/json;charset=utf-8",
      url: "/saveCustomAutomations/".concat(this.currentAutomation,"/", listOfPoints),
      traditional: "true",
      data: null,
      dataType: "json"
    }).done(window.location.href = ("/viewAllCustomAutomations"));

  }
}

// Point class
class Point
{
  constructor(x, y)
  {
    this.x = x;
    this.y = y;
  }

  // distance to a point
  distTo(p)
  {
    return((p.x - this.x) * (p.x - this.x)) + ((p.y - this.y) * (p.y - this.y));
  }
}

// Does a straight line walk and tells you all the x, y values
function generateLineFromPoints(points)
{
  var gradient;
  var segment = 1;
  var y;
  var x = points[0].x;
  for (x; x < points[points.length - 1].x; x++)
  {
    if(x > points[segment].x) { segment += 1}
    gradient = (points[segment].y - points[segment - 1].y) / (points[segment].x - points[segment - 1].x);
    y = gradient * (x - points[segment - 1].x) + points[segment - 1].y
  }
}




var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var canvasScale = document.getElementById("canvasScale").textContent;


var mouse = new Point(0, 0);

var songLength = 1000;
var canvasSize = new Point(1200, 600);


var pList = new PointList([[0, 0], [1, 1]], canvasScale);

// All the mouse bindings, for adding and removing points
canvas.addEventListener('mousemove', function(evt) {pList.updateMouse(evt.x, evt.y); pList.movePoint(); }, false);
canvas.addEventListener('mousedown', function(evt) {if(evt.button == 0){ pList.clickPoint(evt.x, evt.y);} if(evt.button == 1){ pList.deletePoint();} }, false);
canvas.addEventListener('mouseup', function(evt) { if(evt.button == 0){ pList.unclickPoint();} }, false);
canvas.addEventListener('mousewheel', function(evt) { event.preventDefault(); if(evt.deltaY  < 0){ pList.zoomIn(); } else if(evt.deltaY > 0){ pList.zoomOut(); }});
canvas.addEventListener('dblclick', function(evt) { pList.addPoint();}, false);
