from app import functions
from flask import render_template
from flask import request
from app import app


@app.route('/')
def home():
    """ Home Page """
    functions.execute(1) # Update effect descriptions automatically
    return render_template('home.html')

@app.route('/renderSettings')
def renderSettings():
    """ Can change render settings """
    settings = functions.getRenderSettings()
    return render_template('renderSettings.html', settings = settings)

@app.route('/renderSettingsSave', methods=['POST'])
def renderSettingsSave():
    """ AJAX request coming back from Render settings page """
    if request.method == 'POST':
        data = request.get_json()
        functions.saveRenderSettings(data)
        settings = functions.getRenderSettings()
        return render_template('renderSettings.html', settings = settings)

    else:
        # Empty return just in case it was called without AJAX somehow
        return ('', 200)

@app.route('/chooseSong')
def chooseSong():
    """ Displays all songs  in library """
    allSongs = functions.getAllSongs()
    return render_template('chooseSong.html', allSongs = allSongs)

@app.route('/chooseSong/<songTitle>')
def chooseSongVersion(songTitle):
    """ Create or choose version """
    allSongVersions = functions.getAllSongVersions(songTitle)
    return render_template('chooseSongVersion.html', songTitle = songTitle, allVersions = allSongVersions)

@app.route('/addNewSongVersion/<songTitle>')
def addNewSongVersion(songTitle):
    """ Add new version that does not already exist """
    allSongVersions = functions.getAllSongVersions(songTitle)
    return render_template('addNewSongVersion.html', songTitle = songTitle, allVersions = allSongVersions)

@app.route('/updateVersions/<songTitle>/<newSongVersion>', methods=['POST'])
def updateVerisonFromAJAX(songTitle, newSongVersion):
    """ AJAX Coming back from adding a new song version """
    if request.method == 'POST':
        # Save the data, and read it again from the file to update page
        allSongVersions = functions.addNewSongVersion(songTitle, newSongVersion)
        allSongs = functions.getAllSongs()
        return render_template('chooseSongVersion.html', songTitle = songTitle, allVersion = allSongVersions)
    else:
        # Empty return just in case it was called without AJAX somehow
        return ('', 200)



@app.route('/home/<songTitle>/<songVersion>')
def songHome(songTitle, songVersion):
    """ Home page for a specific song """
    # so when we play a song it chooses the right file
    functions.updateCurrentSong(songTitle, songVersion)
    return render_template('songHome.html', songTitle = songTitle, songVersion = songVersion)

@app.route('/runProgramFromSongHome/<songTitle>/<songVersion>')
def runProgramFromSongHome(songTitle, songVersion):
    """ Starts the CPP program """
    functions.runProgram()
    return render_template('songHome.html', songTitle = songTitle, songVersion = songVersion)



@app.route('/editSong/<songTitle>/<songVersion>')
@app.route('/editSong/<songTitle>/<songVersion>/<playbackStart>/<playbackSpeed>')
def editSong(songTitle, songVersion, playbackStart = "0.0", playbackSpeed = "1.0"):
    """ Calls the main page to add and change effects """
    data, fx = functions.getAutomationData(songTitle, songVersion)
    instruments = functions.getAllInstruments(songTitle)
    customAutomations = functions.getCustomAutomations()
    images = functions.getAllImages(songTitle)
    return render_template('editSong.html', songTitle = songTitle, songVersion = songVersion, data = data, fx = fx, instruments = instruments, customAutomations = customAutomations, playbackStart = playbackStart, images = images, playbackSpeed = playbackSpeed)

@app.route('/runProgramFromEditSong/<songTitle>/<songVersion>')
@app.route('/runProgramFromEditSong/<songTitle>/<songVersion>/<playbackStart>/<playbackSpeed>')
def runProgramFromEditSong(songTitle, songVersion, playbackStart = 0.0, playbackSpeed = 1.0):
    """ Called only from the main editing page """
    error = functions.runProgram()
    status = functions.checkSongStatus(songTitle, songVersion)
    print("error:")
    print(error)
    print(status)
    data, fx = functions.getAutomationData(songTitle, songVersion)
    instruments = functions.getAllInstruments(songTitle)
    customAutomations = functions.getCustomAutomations()
    images = functions.getAllImages(songTitle)
    return render_template('editSong.html', songTitle = songTitle, songVersion = songVersion, data = data, fx = fx, instruments = instruments, customAutomations = customAutomations, playbackStart = playbackStart, images = images, playbackSpeed = playbackSpeed, status = status, errorMessage = error)


@app.route('/editSong/<songTitle>/<songVersion>/<playbackStart>/<playbackSpeed>', methods=['POST'])
def updateSongFromAJAX(songTitle, songVersion, playbackStart, playbackSpeed):
    if request.method == 'POST':

        data = request.get_json()
        # Save the data, and read it again from the file to update page
        data, fx, status = functions.saveAutomationData(songTitle, songVersion, data, playbackStart, playbackSpeed)
        print(status)
        instruments = functions.getAllInstruments(songTitle)
        customAutomations = functions.getCustomAutomations()
        images = functions.getAllImages(songTitle)
        return render_template('editSong.html', songTitle = songTitle, songVersion = songVersion, data = data, fx = fx, instruments = instruments, customAutomations = customAutomations, playbackStart = 0.0, images = images, playbackSpeed = playbackSpeed)
    else:
        # Empty return just in case it was called without AJAX somehow
        return ('', 200)



@app.route('/viewAllCustomAutomations')
def viewAllCustomAutomations():
    """ Fullscreen editing page for automations """
    names, data = functions.getCustomAutomations()
    return render_template('editCustomAutomation.html', names = names, data = data)

@app.route('/addNewCustomAutomation')
def addNewCustomAutomation():
    """ Likewise with the add new song version, does not allow already exisiting names """
    allAutomationNames = functions.getCustomAutomations()[0]
    return render_template('addNewCustomAutomation.html', allNames = allAutomationNames)


@app.route('/updateCustomAutomations/<newCustomAutomation>', methods=['POST'])
def updateCustomAutomationFromAJAX(newCustomAutomation):
    """ AJAX Returning from creating a new custom automation """
    if request.method == 'POST':
        # Save the data, and read it again from the file to update page
        names, data = functions.getCustomAutomations(newCustomAutomation)
        return render_template('editCustomAutomation.html', names = names, data = data)
    else:
        # Empty return just in case it was called without AJAX somehow
        return ('', 200)

@app.route('/saveCustomAutomations/<name>/<data>', methods=['POST'])
def saveCustomAutomationFromAJAX(name, data):
    """ AJAX Returning from saving an existing custom automation """
    if request.method == 'POST':
        # Save the data, and read it again from the file to update page
        names, data = functions.getCustomAutomations(name, data)
        return render_template('editCustomAutomation.html', names = names, data = data)
    else:
        # Empty return just in case it was called without AJAX somehow
        return ('', 200)


@app.route('/viewAllEffects')
def viewAllEffects():
    """ Displays all available effects in the system """
    fx = functions.getAllEffects()
    return render_template('viewAllEffects.html', fx = fx)

@app.route('/viewAllVideos')
def viewAllVideos():
    """ Show all song versions in one place """
    songVersions = []
    songs = functions.getAllSongs()
    for s in songs:
        songVersions.append(functions.getAllSongVersions(s))
    return render_template('viewAllVideos.html', songs = songs, songVersions = songVersions)

@app.route('/runProgramFromAllVideos/<songTitle>/<songVersion>')
def runProgramFromAllVideos(songTitle, songVersion):
    """ We need a seperate running function from each starting page so we know where to return to """
    functions.updateCurrentSong(songTitle, songVersion)
    functions.runProgram()
    songVersions = []
    songs = functions.getAllSongs()
    for s in songs:
        songVersions.append(functions.getAllSongVersions(s))
    return render_template('viewAllVideos.html', songs = songs, songVersions = songVersions)

@app.route("/generateData")
@app.route("/generateData/<action>")
def genMetadata(action = 0):
    """ Page to update metadata """
    if(action == 0):
        text = ""
    else:
        text = functions.execute(action)
    return render_template('genData.html', text = text)


@app.route("/about")
def about():
    """ About Page """
    return render_template('about.html')
